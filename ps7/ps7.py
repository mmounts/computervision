"""Problem Set 7: Particle Filter Tracking."""

import numpy as np
import cv2
import math

import os

# I/O directories
input_dir = "input"
output_dir = "output"


# Assignment code
class ParticleFilter(object):
    """A particle filter tracker, encapsulating state, initialization and update methods."""

    def __init__(self, frame, template, **kwargs):
        """Initialize particle filter object.

        Parameters
        ----------
            frame: color BGR uint8 image of initial video frame, values in [0, 255]
            template: color BGR uint8 image of patch to track, values in [0, 255]
            kwargs: keyword arguments needed by particle filter model, including:
            - num_particles: number of particles
        """
        self.template = template
        self.num_particles = kwargs.get('num_particles', 100)  # extract num_particles (default: 100)
        self.sigma_MSE = kwargs.get('sigma_MSE', 10)
        self.diffuse_sigma = kwargs.get('diffuse_sigma', 7)
        self.start_box = kwargs.get('start_box', {'x':0, 'y':0, 'w':frame.shape[1], 'h': frame.shape[0]})
        self.center_offsets = [int(self.start_box['w'] / 2), int(self.start_box['h'] / 2)]
        # self.center_offsets = [self.center_offsets[0] + self.center_offsets[0] % 2, self.center_offsets[1] + self.center_offsets[1] % 2]

        self.start_center = {'x': self.start_box['x'] + self.center_offsets[0], 'y': self.start_box['y'] + self.center_offsets[1]}
        self.particles = [[part[0] + self.start_center['x'], part[1] + self.start_center['y'], 1.0/self.num_particles] for part in np.random.randn(self.num_particles, 2) * 2]
        self.particles = np.array(self.particles)
        self.current_center = [self.start_center['x'], self.start_center['y']]
        # print self.start_center, self.particles
        # TODO: Your code here - extract any additional keyword arguments you need and initialize state

    def set_weighted_center(self):
        total_weight = 0
        total_u = 0
        total_v = 0

        for part in self.particles:
            total_u += (part[0] * part[2])
            total_v += (part[1] * part[2])
            total_weight += part[2]

        self.current_center = [int(total_u / total_weight), int(total_v / total_weight)]

    def get_weighted_mean_diff(self):
        total_weight = 0
        total_sum_diff = 0

        for part in self.particles:
            total_sum_diff += np.linalg.norm(part[0:2] - self.current_center) * part[2]
            total_weight += part[2]

        return total_sum_diff / total_weight
        # print self.current_center, self.particles[:, 0:2], self.particles[:,0:2] - self.current_center
        # pt_dis_sum = np.linalg.norm(self.particles[:,0:2]-self.current_center)
        # pt_dis_mean = pt_dis_sum / self.num_particles
        # print pt_dis_sum, pt_dis_mean

    def get_point_template(self, frame, point):
        point_up_left = [max(int(point[0] - self.center_offsets[0]), 0), max(int(point[1] - self.center_offsets[1]), 0)]
        point_template = frame[point_up_left[1]:point_up_left[1] + self.template.shape[0], point_up_left[0]:point_up_left[0] + self.template.shape[1]]
        return point_template


    def process(self, frame):
        """Process a frame (image) of video and update filter state.

        Parameters
        ----------
            frame: color BGR uint8 image of current video frame, values in [0, 255]
        """
        # Your code here - use the frame as a new observation (measurement) and update model
        new_particles = []
        total_weight = 0
        particle_weights = []

        # Add noise to current particles
        noise = np.random.randn(self.num_particles, 2) * self.diffuse_sigma
        self.particles[:,0:2] = self.particles[:,0:2] + noise

        # self.particles = [[p[0] + n[0], p[1] + n[1], p[2]] for p in self.particles for n in noise]

        for part in self.particles:
            part_template = self.get_point_template(frame, part)
            if part_template.shape != self.template.shape:
                # print part_template.shape, self.template.shape
                continue;

            mse = ((part_template - self.template) ** 2).mean(axis=None)
            prob = math.exp(-mse / (2 * (self.sigma_MSE**2)))
            part[2] = prob;

        total_weight = np.sum(self.particles[:,2])
        self.particles[:,2] = self.particles[:,2] / total_weight
        # print self.particles[0], total_weight, self.particles[:, 2]

        self.set_weighted_center()

        part_selections = np.random.multinomial(self.num_particles, self.particles[:,2])
        # print part_selections
        for part_idx in range(len(part_selections)):
            for count in range(part_selections[part_idx]):
                new_particles.append(self.particles[part_idx])

        self.particles = np.array(new_particles)



    def render(self, frame_out):
        """Visualize current particle filter state.

        Parameters
        ----------
            frame_out: copy of frame to overlay visualization on
        """
        # print 'rendering'
        # Your code here - draw particles, tracking window and a circle to indicate spread
        for part in self.particles:
            cv2.circle(frame_out, (int(part[0]), int(part[1])), 1, (0,255,0), 1)
        cv2.circle(frame_out, (int(self.current_center[0]), int(self.current_center[1])), 5, (255,255,255), 2)
        cv2.rectangle(frame_out,
            (int(self.current_center[0] - self.center_offsets[0]), int(self.current_center[1] - self.center_offsets[1])),
            (int(self.current_center[0] + self.center_offsets[0]), int(self.current_center[1] + self.center_offsets[1])),
            (255,0,0))

        pt_dis_mean = self.get_weighted_mean_diff()
        cv2.circle(frame_out, (int(self.current_center[0]), int(self.current_center[1])), int(pt_dis_mean), (0,0,255), 2)

        # Note: This may not be called for all frames, so don't do any model updates here!


class AppearanceModelPF(ParticleFilter):
    """A variation of particle filter tracker that updates its appearance model over time."""

    def __init__(self, frame, template, **kwargs):
        """Initialize appearance model particle filter object (parameters same as ParticleFilter)."""
        super(AppearanceModelPF, self).__init__(frame, template, **kwargs)  # call base class constructor
        # Your code here - additional initialization steps, keyword arguments
        self.cur_target = template
        self.running_target = template
        self.template_alpha = kwargs.get('template_alpha', .5)

    # Override process() to implement appearance model update
    def process(self, frame):
        super(AppearanceModelPF, self).process(frame)
        self.cur_target = self.get_point_template(frame, self.current_center)
        if self.cur_target.shape == self.template.shape:
            self.template = self.template_alpha * self.cur_target + (1-self.template_alpha) * self.template


    # Not doing
    # TODO: Override render() if desired (shouldn't have to, ideally)


# Driver/helper code
def get_template_rect(rect_filename, x_padding=0, y_padding=0):
    """Read rectangular template bounds from given file.

    The file must define 4 numbers (floating-point or integer), separated by whitespace:
    <x> <y>
    <w> <h>

    Parameters
    ----------
        rect_filename: path to file defining template rectangle

    Returns
    -------
        template_rect: dictionary specifying template bounds (x, y, w, h), as float or int

    """
    with open(rect_filename, 'r') as f:
        values = [float(v) for v in f.read().split()]
        values[0] -= x_padding; values[1] -= y_padding
        values[2] += x_padding*2; values[3] += y_padding*2
        return dict(zip(['x', 'y', 'w', 'h'], values[0:4]))


def run_particle_filter(pf_class, video_filename, template_rect, save_frames={}, **kwargs):
    """Instantiate and run a particle filter on a given video and template.

    Create an object of type pf_class, passing in initial video frame,
    template (extracted from first frame using template_rect), and any keyword arguments.

    Parameters
    ----------
        pf_class: particle filter class to instantiate (e.g. ParticleFilter)
        video_filename: path to input video file
        template_rect: dictionary specifying template bounds (x, y, w, h), as float or int
        save_frames: dictionary of frames to save {<frame number>|'template': <filename>}
        kwargs: arbitrary keyword arguments passed on to particle filter class
    """

    # Open video file
    video = cv2.VideoCapture(video_filename)

    # Initialize objects
    template = None
    pf = None
    frame_num = 0

    # Loop over video (till last frame or Ctrl+C is presssed)
    while True:
        try:
            # Try to read a frame
            okay, frame = video.read()
            if not okay:
                break  # no more frames, or can't read video

            # Extract template and initialize (one-time only)
            if template is None:
                # print 'creating template'
                template = frame[int(template_rect['y']):int(template_rect['y'] + template_rect['h']),
                                 int(template_rect['x']):int(template_rect['x'] + template_rect['w'])]
                # template = get_scaled_gray_frame(template)
                if 'template' in save_frames:
                    cv2.imwrite(save_frames['template'], template)
                pf = pf_class(frame, template, **kwargs)

            # Process frame
            # print frame_num
            # pf.process(get_scaled_gray_frame(frame))  # TODO: implement this!
            pf.process(frame)  # TODO: implement this!

            # Render and save output, if indicated
            if frame_num in save_frames:
                frame_out = frame.copy()
                pf.render(frame_out)
                cv2.imwrite(save_frames[frame_num], frame_out)

            # Update frame number
            frame_num += 1
        except KeyboardInterrupt:  # press ^C to quit
            break

def get_scaled_gray_frame(frame):
    return cv2.cvtColor(frame, cv2.COLOR_RGB2GRAY)

def main():
    # Note: Comment out parts of this code as necessary

    # 1a
    # Implement ParticleFilter
    debate_template = get_template_rect(os.path.join(input_dir, "pres_debate.txt"), -10, 0)
    run_particle_filter(ParticleFilter,  # particle filter model class
        os.path.join(input_dir, "pres_debate.avi"),  # input video
        debate_template,  # suggested template window (dict)
        # Note: To specify your own window, directly pass in a dict: {'x': x, 'y': y, 'w': width, 'h': height}
        {
            'template': os.path.join(output_dir, 'ps7-1-a-1.png'),
            # 1: os.path.join(output_dir, 'ps7-1-a-1-test1.png'),
            # 5: os.path.join(output_dir, 'ps7-1-a-1-test5.png'),
            # 10: os.path.join(output_dir, 'ps7-1-a-1-test10.png'),
            # 15: os.path.join(output_dir, 'ps7-1-a-1-test15.png'),
            # 20: os.path.join(output_dir, 'ps7-1-a-1-test20.png'),
            # 25: os.path.join(output_dir, 'ps7-1-a-1-test25.png'),
            28: os.path.join(output_dir, 'ps7-1-a-2.png'),
            # 35: os.path.join(output_dir, 'ps7-1-a-1-test35.png'),
            # 45: os.path.join(output_dir, 'ps7-1-a-1-test45.png'),
            # 55: os.path.join(output_dir, 'ps7-1-a-1-test55.png'),
            # 65: os.path.join(output_dir, 'ps7-1-a-1-test65.png'),
            # 75: os.path.join(output_dir, 'ps7-1-a-1-test75.png'),
            84: os.path.join(output_dir, 'ps7-1-a-3.png'),
            144: os.path.join(output_dir, 'ps7-1-a-4.png')
        },  # frames to save, mapped to filenames, and 'template' if desired
        num_particles=20,
        start_box=debate_template,
        sigma_MSE=3)
    #     # specify other keyword args that your model expects, e.g. measurement_noise=0.2

    # 1b
    # Repeat 1a, but vary template window size and discuss trade-offs (no output images required)
    # Larger window:
    # Cons:
    # Less accurate as more of the blue background get put into the template
    # Slower (more pixels to compare)
    # Pros:
    # More likely for face to show within bounding box (larger margin of error)
    # With enough padding (>150 pixels), the shoulders become a feature which actually increases accuracy

    # Smaller window:
    # Pros:
    # Runs faster
    # Contains only the most important features (eyes/nose/mouth)
    # Getting small enough to only include skintone means the wall is always considered wrong
    # Cons:
    # Getting to small removes useful features (like mouth)

    # I got best performance out of slightly smaller but lopsided (taller than wide) window

    # 1c
    # Repeat 1a, but vary the sigma_MSE parameter (no output images required)
    # Note: To add a parameter, simply pass it in here as a keyword arg and extract it back in __init__()
    # 5: Soooo much more accurate than 10
    # 2: Even better! It even tracked the head turn (with my good lopsided window)
    # 1: Not quite as good as 2, a little too constricted
    # 15: Actually slightly better than 10 (was almost able to track head turn), but not as good as lower numbers
    # 20: Significantly degraded performance compared to 10, no turning head tracking at all


    # 1d
    # Repeat 1a, but try to optimize (minimize) num_particles (no output images required)
    # With optimized settings (sigma_MSE = 3, window shrunk by 20px horizontall and 10px vertically), I was able to
    # go down to 10 points without any degraded performance. 5 had significantly degraded performance.
    # However, 10-30 was not consisten with tracking the face turn. It depended on the exact nature of the noise
    # A larger number (100+) was able to track the head turn slightly better due to more points and noise available
    # to help track the movement. However, between the frames before the head turn there was no accuracy difference
    # between 10 and 100+, but the later too much longer to compute.
    # Going much higher (500 points) did nothing different when compared to 100 points, and so was a waste of computational power

    # 1e
    noisy_template = get_template_rect(os.path.join(input_dir, "noisy_debate.txt"), -10, 0)
    run_particle_filter(ParticleFilter,
        os.path.join(input_dir, "noisy_debate.avi"),
        noisy_template,
        {
            # 1: os.path.join(output_dir, 'ps7-1-e-1-test1.png'),
            14: os.path.join(output_dir, 'ps7-1-e-1.png'),
            32: os.path.join(output_dir, 'ps7-1-e-2.png'),
            46: os.path.join(output_dir, 'ps7-1-e-3.png'),
            # 55: os.path.join(output_dir, 'ps7-1-e-1-test55.png'),
            # 65: os.path.join(output_dir, 'ps7-1-e-1-test65.png')
        },
        num_particles=50,
        start_box=noisy_template,
        sigma_MSE=4)  #  Tune parameters so that model can continuing tracking through noise

    # A larger than minimal point set (50 seems right) was required to deal with reseting after the noisy
    # It was also sufficient to not get too confused during the noisy period
    # Smaller sigma_MSE values (4 seemed best) were still better than 10+

    # 2a
    # Implement AppearanceModelPF (derived from ParticleFilter)
    # Run it on pres_debate.avi to track Romney's left hand, tweak parameters to track up to frame 140
    hand_template = {'x':560,'y':425,'w':30,'h':40}
    run_particle_filter(AppearanceModelPF,
        os.path.join(input_dir, "pres_debate.avi"),
        hand_template,
        {
            'template': os.path.join(output_dir, 'ps7-2-a-1.png'),
            # 1: os.path.join(output_dir, 'ps7-2-a-1-test1.png'),
            # 5: os.path.join(output_dir, 'ps7-2-a-1-test5.png'),
            # 9: os.path.join(output_dir, 'ps7-2-a-1-test9.png'),
            15: os.path.join(output_dir, 'ps7-2-a-2.png'),
            # 32: os.path.join(output_dir, 'ps7-2-a-1-test32.png'),
            50: os.path.join(output_dir, 'ps7-2-a-3.png'),
            # 55: os.path.join(output_dir, 'ps7-2-a-1-test55.png'),
            # 65: os.path.join(output_dir, 'ps7-2-a-1-test65.png'),
            # 85: os.path.join(output_dir, 'ps7-2-a-1-test85.png'),
            # 105: os.path.join(output_dir, 'ps7-2-a-1-test105.png'),
            140: os.path.join(output_dir, 'ps7-2-a-4.png'),
            # 150: os.path.join(output_dir, 'ps7-2-a-1-test150.png')
        },
        num_particles=200, #500
        start_box=hand_template,
        sigma_MSE=7, # 5
        diffuse_sigma=15, # 10
        template_alpha=.3) # .3

    # 2b
    # Run AppearanceModelPF on noisy_debate.avi, tweak parameters to track hand up to frame 140
    hand_template = {'x':550,'y':425,'w':30,'h':40}
    run_particle_filter(AppearanceModelPF,
        os.path.join(input_dir, "noisy_debate.avi"),
        hand_template,
        {
            'template': os.path.join(output_dir, 'ps7-2-b-1.png'),
            # 1: os.path.join(output_dir, 'ps7-2-b-1-test1.png'),
            # 5: os.path.join(output_dir, 'ps7-2-b-1-test5.png'),
            # 9: os.path.join(output_dir, 'ps7-2-b-1-test9.png'),
            # 12: os.path.join(output_dir, 'ps7-2-b-1-test12.png'),
            15: os.path.join(output_dir, 'ps7-2-b-2.png'),
            # 32: os.path.join(output_dir, 'ps7-2-b-1-test32.png'),
            50: os.path.join(output_dir, 'ps7-2-b-3.png'),
            # 55: os.path.join(output_dir, 'ps7-2-b-1-test55.png'),
            # 65: os.path.join(output_dir, 'ps7-2-b-1-test65.png'),
            # 85: os.path.join(output_dir, 'ps7-2-b-1-test85.png'),
            # 105: os.path.join(output_dir, 'ps7-2-b-1-test105.png'),
            140: os.path.join(output_dir, 'ps7-2-b-4.png'),
            # 150: os.path.join(output_dir, 'ps7-2-b-1-test150.png')
        },
        num_particles=300, #300 || 500
        start_box=hand_template,
        sigma_MSE=10, # 10
        diffuse_sigma=20, # 15
        template_alpha=.1) # .1 || .05

    """
        3 main changes: needed a lot more points and spread so we wouldn't lose the hand during the noisy parts
        However, most importantly, the template alpha had to be drastically reduced (.05-.1 vs .3) in order
        for the noisy sections to not muddle the rolling template too much.  Sigma_MSE was also increased, but
        this optimization was not as drastic as the others
    """

    # EXTRA CREDIT
    # 3: Use color histogram distance instead of MSE (you can implement a derived class similar to AppearanceModelPF)
    # 4: Implement a more sophisticated model to deal with occlusions and size/perspective changes

    ped_template = get_template_rect(os.path.join(input_dir, "pedestrians.txt"), 0, 0)
    run_particle_filter(AppearanceModelPF,
        os.path.join(input_dir, "pedestrians.avi"),
        ped_template,
        {
            'template': os.path.join(output_dir, 'ps7-4-a-1.png'),
            # 1: os.path.join(output_dir, 'ps7-4-a-1-test1.png'),
            # 5: os.path.join(output_dir, 'ps7-4-a-1-test5.png'),
            # 9: os.path.join(output_dir, 'ps7-4-a-1-test9.png'),
            # 12: os.path.join(output_dir, 'ps7-4-a-1-test12.png'),
            # 15: os.path.join(output_dir, 'ps7-4-a-1.png'),
            # 32: os.path.join(output_dir, 'ps7-4-a-1-test32.png'),
            40: os.path.join(output_dir, 'ps7-4-a-2.png'),
            # 55: os.path.join(output_dir, 'ps7-4-a-1-test55.png'),
            # 65: os.path.join(output_dir, 'ps7-4-a-1-test65.png'),
            # 85: os.path.join(output_dir, 'ps7-4-a-1-test85.png'),
            # 100: os.path.join(output_dir, 'ps7-4-a-1-test105.png'),
            140: os.path.join(output_dir, 'ps7-4-a-3.png'),
            # 150: os.path.join(output_dir, 'ps7-4-a-1-test150.png'),
            240: os.path.join(output_dir, 'ps7-4-a-4.png')
        },
        num_particles=300, #300 || 500
        start_box=ped_template,
        sigma_MSE=10, # 10
        diffuse_sigma=5, # 5
        template_alpha=.1) # .1 || .05


if __name__ == "__main__":
    main()
