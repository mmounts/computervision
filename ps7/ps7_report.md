Mitchell Mounts

GA ID: 903176000

Computer Vision Fall 2015 OMSCS

PS7 Report

**1:**

1-a:

ps7-1-a-1.png
![ps7-1-a-1]

ps7-1-a-2.png
![ps7-1-a-2]

ps7-1-a-3.png
![ps7-1-a-3]

ps7-1-a-4.png
![ps7-1-a-4]

1-b:

Larger window:

  Cons:

    * Less accurate as more of the blue background gets put into the template
    * Slower (more pixels to compare)

  Pros:

    * More likely for face to show within bounding box (larger margin of error)
    * With enough padding (>150 pixels), the shoulders become a feature which actually increases accuracy

Smaller window:

  Pros:

    * Runs faster
    * Contains only the most important features (eyes/nose/mouth)
    * Getting small enough to only include skintone means the wall is outside of the comparison

  Cons:

    * Getting too small removes useful features (like mouth)

    I got best performance out of slightly smaller but lopsided (taller than wide) window, which captured the most important facial features without capturing any wall color.

1-c:

(Note: text comparison is vs default sigma value of 10)
* 5: Much more accurate than 10
* 2: Even better than 5! It even tracked the head turn (with a good, lopsided (tall and skinnier) window)
* 1: Not quite as good as 2, too constricted
* 15: Actually slightly better than 10 (was almost able to track head turn), but not as good as lower numbers
* 20: Significantly degraded performance compared to 10, no turning head tracking at all

Clearly 10 is not the best choice, with advantages with both slightly larger and smaller values.
However, both over and under constraining the tracker cause issues by restricting the accuracy of the probability values.

1-d:

With optimized settings (sigma_MSE = 3, window shrunk by 20px horizontally and 10px vertically), I was able to
go down to 10 points without any degraded performance. 5 had significantly degraded performance.
However, 10-30 was not consistent with tracking the face turn. It depended on the exact nature of the noise.
A larger number (100+) was able to track the head turn slightly better due to more points and noise available
to help track the movement. However, between the frames before the head turn there was no accuracy difference
between 10 and 100+, but the later took much longer to compute.
Going much higher (500 points) did nothing different when compared to 100 points, and so was a waste of computational power.

1-e:

ps7-1-e-1.png
![ps7-1-e-1]

ps7-1-e-2.png
![ps7-1-e-2]

ps7-1-e-3.png
![ps7-1-e-3]

Text Response:
A larger than minimal point set (50 seems right) was required to deal with reseting after the noisy segments.
It was also sufficient to not get too confused during the noisy period itself.
Smaller sigma_MSE values (4 seemed best) were still better than 10+


**2:**

2-a:

ps7-2-a-1.png
![ps7-2-a-1]

ps7-2-a-2.png
![ps7-2-a-2]

ps7-2-a-3.png
![ps7-2-a-3]

ps7-2-a-4.png
![ps7-2-a-4]


2-b:

ps7-2-b-1.png
![ps7-2-b-1]

ps7-2-b-2.png
![ps7-2-b-2]

ps7-2-b-3.png
![ps7-2-b-3]

ps7-2-b-4.png
![ps7-2-b-4]

Text Response:

3 main changes compared to 2-a: 
I needed a lot more points and spread so we wouldn't lose the hand during the noisy parts (300 vs 100-200)
However, most importantly, the template alpha had to be drastically reduced (.05-.1 vs .3) in order
for the noisy sections to not muddle the rolling template too much.  Sigma_MSE was also increased (10 vs 7), but
this optimization was not as drastic as the others


**3:**
Not Done


**4:**

4-a:

ps7-4-a-1.png
![ps7-4-a-1]

ps7-4-a-2.png
![ps7-4-a-2]

ps7-4-a-3.png
![ps7-4-a-3]

ps7-4-a-4.png
![ps7-4-a-4]


4-b:
Not Done

[ps7-1-a-1]: ./output/ps7-1-a-1.png  "ps7-1-a-1"
[ps7-1-a-2]: ./output/ps7-1-a-2.png  "ps7-1-a-2"
[ps7-1-a-3]: ./output/ps7-1-a-3.png  "ps7-1-a-3"
[ps7-1-a-4]: ./output/ps7-1-a-4.png  "ps7-1-a-4"
[ps7-1-e-1]: ./output/ps7-1-e-1.png  "ps7-1-e-1"
[ps7-1-e-2]: ./output/ps7-1-e-2.png  "ps7-1-e-2"
[ps7-1-e-3]: ./output/ps7-1-e-3.png  "ps7-1-e-3"
[ps7-2-a-1]: ./output/ps7-2-a-1.png  "ps7-2-a-1"
[ps7-2-a-2]: ./output/ps7-2-a-2.png  "ps7-2-a-2"
[ps7-2-a-3]: ./output/ps7-2-a-3.png  "ps7-2-a-3"
[ps7-2-a-4]: ./output/ps7-2-a-4.png  "ps7-2-a-4"
[ps7-2-b-1]: ./output/ps7-2-b-1.png  "ps7-2-b-1"
[ps7-2-b-2]: ./output/ps7-2-b-2.png  "ps7-2-b-2"
[ps7-2-b-3]: ./output/ps7-2-b-3.png  "ps7-2-b-3"
[ps7-2-b-4]: ./output/ps7-2-b-4.png  "ps7-2-b-4"
[ps7-4-a-1]: ./output/ps7-4-a-1.png  "ps7-4-a-1"
[ps7-4-a-2]: ./output/ps7-4-a-2.png  "ps7-4-a-2"
[ps7-4-a-3]: ./output/ps7-4-a-3.png  "ps7-4-a-3"
[ps7-4-a-4]: ./output/ps7-4-a-4.png  "ps7-4-a-4"