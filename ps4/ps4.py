"""Problem Set 4: Geometry."""

import numpy as np
import cv2
from math import sqrt
import sys

import os

# I/O directories
input_dir = "input"
output_dir = "output"

# Input files
PIC_A = "pic_a.jpg"
PIC_A_2D = "pts2d-pic_a.txt"
PIC_A_2D_NORM = "pts2d-norm-pic_a.txt"
PIC_B = "pic_b.jpg"
PIC_B_2D = "pts2d-pic_b.txt"
SCENE = "pts3d.txt"
SCENE_NORM = "pts3d-norm.txt"

# Utility code
def read_points(filename):
    """Read point data from given file and return as NumPy array."""
    with open(filename) as f:
        lines = f.readlines()
        pts = []
        for line in lines:
            pts.append(map(float, line.split()))
    return np.array(pts)


# Assignment code
def solve_least_squares(pts3d, pts2d):
    """Solve for transformation matrix M that maps each 3D point to corresponding 2D point using the least squares method.

    Parameters
    ----------
        pts3d: 3D (object) points, NumPy array of shape (N, 3)
        pts2d: corresponding 2D (image) points, NumPy array of shape (N, 2)

    Returns
    -------
        M: transformation matrix, NumPy array of shape (3, 4)
        error: sum of squared residuals of all points
    """

    # TODO: Your code here
    # print pts3d, pts2d
    A_matrix = []
    B_matrix = []
    for x in range(len(pts3d)):
        pt3d = pts3d[x]
        pt2d = pts2d[x]
        # Append 2 rows for A matrix
        A_matrix.append([pt3d[0], pt3d[1], pt3d[2], 1, 0, 0, 0, 0,
                          -(pt2d[0]*pt3d[0]), -(pt2d[0]*pt3d[1]), -(pt2d[0]*pt3d[2])])
        A_matrix.append([0, 0, 0, 0, pt3d[0], pt3d[1], pt3d[2], 1,
                          -(pt2d[1]*pt3d[0]), -(pt2d[1]*pt3d[1]), -(pt2d[1]*pt3d[2])])
        # Since m(2,3) == 1, Move u(i) and v(i) over to right hand side of equation (B matrix)
        B_matrix.append([pt2d[0]])
        B_matrix.append([pt2d[1]])

    # print A_matrix
    A_matrix = np.array(A_matrix)
    # print A_matrix.shape
    B_matrix = np.array(B_matrix)
    # print B

    raw_M, error, rank, sing = np.linalg.lstsq(A_matrix, B_matrix)
    # don't try to scale here, answer is correct but needs scaled by -.5679 something.
    raw_M = raw_M.tolist()
    raw_M.append([1])
    raw_M = np.array(raw_M)
    raw_M.shape = (3,4)
    # print raw_M
    # print raw_M, error
    return raw_M, error


def project_points(pts3d, M):
    """Project each 3D point to 2D using matrix M.

    Parameters
    ----------
        pts3d: 3D (object) points, NumPy array of shape (N, 3)
        M: projection matrix, NumPy array of shape (3, 4)

    Returns
    -------
        pts2d_projected: projected 2D points, NumPy array of shape (N, 2)
    """
    # print M
    pts2d_projected = []
    for p in pts3d:
        p_homo = p.tolist(); p_homo = p_homo + [1]; p_homo = np.array(p_homo)
        # print M, p_homo

        pt2d = np.dot(M, p_homo)
        # print pt2d
        pt2d = [pt2d[0] / pt2d[2], pt2d[1] / pt2d[2]]
        # print pt2d
        pts2d_projected.append(pt2d)

    pts2d_projected = np.array(pts2d_projected)
    return pts2d_projected


def get_residuals(pts2d, pts2d_projected):
    """Compute residual error for each point.

    Parameters
    ----------
        pts2d: observed 2D (image) points, NumPy array of shape (N, 2)
        pts2d_projected: 3D (object) points projected to 2D, NumPy array of shape (N, 3)

    Returns
    -------
        residuals: residual error for each point (L2 distance between observed and projected 2D points)
    """
    residuals = []

    for idx in range(len(pts2d)):
        p_real = pts2d[idx]; p_pro = pts2d_projected[idx]
        squares = ((p_real - p_pro)**2)
        res = sqrt(np.sum(squares))
        residuals.append(res)

    residuals = np.array(residuals)
    return residuals


def calibrate_camera(pts3d, pts2d):
    """Find the best camera projection matrix given corresponding 3D and 2D points.

    Parameters
    ----------
        pts3d: 3D (object) points, NumPy array of shape (N, 3)
        pts2d: corresponding 2D (image) points, NumPy array of shape (N, 2)

    Returns
    -------
        bestM: best transformation matrix, NumPy array of shape (3, 4)
        error: sum of squared residuals of all points for bestM
    """
    bestM = []; error = sys.maxint;
    all_average_res = [];
    test_sizes = [8, 12, 16]
    rand_seed_base = np.random.randint(0,10000)
    # For each k in [8, 12, 16]
    for k in test_sizes:
        k_avr_res = []
        # Repeat 10 times
        for x in range(10):
            # Controlled shuffle to randomly select training and test points
            np.random.seed(rand_seed_base + x)
            rand_state = np.random.get_state()
            np.random.shuffle(pts3d)
            np.random.set_state(rand_state)
            np.random.shuffle(pts2d)
            # print pts3d[:k], pts2d[:k]

            cur_M, cur_error = solve_least_squares(pts3d[:k], pts2d[:k])
            # print cur_M

            pro_points = project_points(pts3d[k+1:k+5], cur_M)
            cur_res = get_residuals(pts2d[k+1:k+5], pro_points)
            cur_avr = np.average(cur_res)
            # print cur_avr
            k_avr_res.append(cur_avr)
            if cur_avr < error:
                error = cur_avr
                bestM = cur_M
        all_average_res.append(k_avr_res)

    # print 'all avr res:\n', all_average_res
    # NOTE: Use the camera calibration procedure in the problem set
    # print bestM, error
    return bestM, error


def compute_fundamental_matrix(pts2d_a, pts2d_b):
    """Compute fundamental matrix given corresponding points from 2 images of a scene.

    Parameters
    ----------
        pts2d_a: 2D points from image A, NumPy array of shape (N, 2)
        pts2d_b: corresponding 2D points from image B, NumPy array of shape (N, 2)

    Returns
    -------
        F: the fundamental matrix
    """
    # print pts2d_a, pts2d_b
    l_matrix = []
    r_matrix = []
    for idx in range(len(pts2d_a)):
        pt_a = pts2d_a[idx]; pt_b = pts2d_b[idx]
        l_matrix.append([pt_a[0] * pt_b[0], pt_a[0] * pt_b[1], pt_a[0],
                         pt_a[1] * pt_b[0], pt_a[1] * pt_b[1], pt_a[1],
                         pt_b[0], pt_b[1]])
        r_matrix.append(-1)

    l_matrix = np.array(l_matrix); r_matrix = np.array(r_matrix)

    raw_F, error, rank, sing = np.linalg.lstsq(l_matrix, r_matrix)
    # print raw_F
    raw_F = raw_F.tolist(); raw_F.append(1); F = np.array(raw_F)
    F.shape = (3, 3)
    # print F

    return F


# Driver code
def main():
    """Driver code."""

    # 1a
    # Read points
    pts3d_norm = read_points(os.path.join(input_dir, SCENE_NORM))
    pts2d_norm_pic_a = read_points(os.path.join(input_dir, PIC_A_2D_NORM))

    # Solve for transformation matrix using least squares
    M, error = solve_least_squares(pts3d_norm, pts2d_norm_pic_a)
    # print 'M:\n', M

    # Project 3D points to 2D
    pts2d_projected = project_points(pts3d_norm, M)
    # print pts2d_projected

    # Compute residual error for each point
    residuals = get_residuals(pts2d_norm_pic_a, pts2d_projected)

    # Print the <u, v> projection of the last point, and the corresponding residual
    # print 'projected point: ', pts2d_projected[-1]
    # print 'point residual: ', residuals[-1]

    # # 1b
    # # Read points
    pts3d = read_points(os.path.join(input_dir, SCENE))
    pts2d_pic_b = read_points(os.path.join(input_dir, PIC_B_2D))
    # NOTE: These points are not normalized

    # Use the functions from 1a to implement calibrate_camera() and find the best transform (bestM)
    best_M, error = calibrate_camera(pts3d, pts2d_pic_b.copy())
    # print 'best M:\n', best_M

    # 1c
    # Compute the camera location using bestM
    # Q = np.array(M[:3,:3])
    Q = np.array(best_M[:3, :3])
    # print best_M
    # m4 = np.array(M[:, 3:4])
    m4 = np.array(best_M[:, 3:4])
    C = np.dot(-1 * np.linalg.inv(Q), m4)
    # print Q, m4
    # print 'camera center:', C

    # 2a
    # Implement compute_fundamental_matrix() to find the raw fundamental matrix
    pts2d_pic_a = read_points(os.path.join(input_dir, PIC_A_2D))
    F = compute_fundamental_matrix(pts2d_pic_a, pts2d_pic_b)
    # print 'F-hat: ', F

    # 2b
    # Reduce the rank of the fundamental matrix
    u, s, v = np.linalg.svd(F)
    s[2] = 0
    F_new = np.dot(np.dot(u, np.diag(s)), v)
    # print 'F: ', F_new

    # 2c
    # Draw epipolar lines
    pic_a = cv2.imread(os.path.join(input_dir, PIC_A))
    pic_b = cv2.imread(os.path.join(input_dir, PIC_B))
    # print pic_a.shape, pic_b.shape

    F_t = np.transpose(F_new)
    I_L = np.cross([0, 0, 1], [0, pic_a.shape[0], 1])
    I_R = np.cross([pic_a.shape[1], 0, 1], [pic_a.shape[1], pic_a.shape[0], 1])
    # print I_L, I_R
    b_lns = []
    for pt_b in pts2d_pic_b:
        pt_b = pt_b.tolist(); pt_b.append(1); pt_b = np.array(pt_b)
        # print pt_a
        l = np.dot(F_new, pt_b)
        # Get homogenous left and right line points
        P_il = np.cross(l, I_L)
        P_ir = np.cross(l, I_R)
        # Make points imhomogenous
        # print l, P_il, P_ir
        P_il = (int(P_il[0]/P_il[2]), int(P_il[1]/P_il[2]))
        P_ir = (int(P_ir[0]/P_ir[2]), int(P_ir[1]/P_ir[2]))
        cv2.line(pic_a, P_il, P_ir, (255,125,125), 1)

        b_lns.append(l)

    for pt_a in pts2d_pic_a:
        pt_a = pt_a.tolist(); pt_a.append(1); pt_a = np.array(pt_a)
        l = np.dot(F_t, pt_a)
        P_il = np.cross(l, I_L)
        P_ir = np.cross(l, I_R)
        P_il = (int(P_il[0]/P_il[2]), int(P_il[1]/P_il[2]))
        P_ir = (int(P_ir[0]/P_ir[2]), int(P_ir[1]/P_ir[2]))
        # print P_il, P_ir
        cv2.line(pic_b, P_il, P_ir, (255,125,125), 1)

    cv2.imwrite(os.path.join(output_dir, 'ps4-2-c-1.png'), pic_a)
    cv2.imwrite(os.path.join(output_dir, 'ps4-2-c-2.png'), pic_b)


if __name__ == '__main__':
    main()
