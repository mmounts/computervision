import cv2
import numpy
import scipy

img1 = cv2.imread('./output/ps1-1-a-1.png')
img2 = cv2.imread('./output/ps1-1-a-2.png')
b1,g1,r1 = cv2.split(img1)
b2,g2,r2 = cv2.split(img2)

# Create 2.a output
ps1_2_a = cv2.merge((r1,g1,b1))
cv2.imwrite('./output/ps1-2-a-1.png', ps1_2_a)

# Write 2.b output
cv2.imwrite('./output/ps1-2-b-1.png', g1)
# 2.d Green is better, color difference between surfaces + edges are better preserved

# Write 2.c output
cv2.imwrite('./output/ps1-2-c-1.png', r1)

# 3.a
img1Center = g1[99:199,149:249]
img2MonoClone = g2.copy()
img2MonoClone[149:249,99:199] = img1Center
# cv2.imwrite('./output/ps1-3-a-1.png', img2MonoClone)

# 4.a
mean = g1.mean()
min = g1.min()
max = g1.max()
std = g1.std()
#print mean, min, max, std
# 108.456916667 0 255 71.8016330629

# 4.b
g1Clone = g1.copy().astype(float)
g1Clone = (((g1Clone-mean)/std) * 10) + mean
# print g1Clone.min(), g1Clone.max()
cv2.imwrite('./output/ps1-4-b-1-new.png', cv2.normalize(g1Clone, alpha=0, beta=255, norm_type=cv2.cv.CV_MINMAX))

# 4.c
def shift2(arr):
    return numpy.append(arr[2:], [0,0])

g1Shifted = g1.copy()
g1Shifted = numpy.apply_along_axis(shift2, 1, g1Shifted)
cv2.imwrite('./output/ps1-4-c-1.png', g1Shifted)

# 4.d
g1ShiftSub = numpy.subtract(g1.copy().astype(float), g1Shifted.copy().astype(float))
# print g1ShiftSub.min(), g1ShiftSub.max()
cv2.imwrite('./output/ps1-4-d-1.png', cv2.normalize(g1ShiftSub,  alpha=0, beta=255, norm_type=cv2.cv.CV_MINMAX))

# 5.a
noise = numpy.random.normal(0,8, g1.shape)
# print noise.min(), noise.max()
g1Noise = g1.copy().astype(float)
g1Noise = numpy.add(g1Noise, noise)
noisyImg1 = img1.copy()
noisyImg1[:,:,1] = cv2.normalize(g1Noise, alpha=0, beta=255, norm_type=cv2.cv.CV_MINMAX)
cv2.imwrite('./output/ps1-5-a-1.png', noisyImg1)

# 5.b
b1Noise = b1.copy().astype(float)
b1Noise = numpy.add(b1Noise, noise)
noisyImg1b = img1.copy()
noisyImg1b[:,:,0] = cv2.normalize(b1Noise, alpha=0, beta=255, norm_type=cv2.cv.CV_MINMAX)
cv2.imwrite('./output/ps1-5-b-1.png', noisyImg1b)