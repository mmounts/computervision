Mitchell Mounts

GA ID: 903176000

Computer Vision Fall 2015 OMSCS

PS8 Report

**1:**

1-a:

ps8-1-a-1.png
![ps8-1-a-1]

ps8-1-a-2.png
![ps8-1-a-2]

ps8-1-a-3.png
![ps8-1-a-3]


1-b:

ps8-1-b-1.png
![ps8-1-b-1]

ps8-1-b-2.png
![ps8-1-b-2]

ps8-1-b-3.png
![ps8-1-b-3]

Text Response:

I used a tau of 25 (the standard that I chose for most of my images in Question 2) for image both 1-b-2 and 1-b-3. However, 1-b-1 was best captured with a tau of 30.


**2:**

2-a:

Confusion matrix (unscaled central moments):-

    [[ 0.88888889  0.          0.11111111]
    [ 0.          0.88888889  0.11111111]
    [ 0.          0.11111111  0.88888889]]


Confusion matrix (scaled central moments):-

    [[ 1.          0.          0.        ]
    [ 0.          1.          0.        ]
    [ 0.11111111  0.11111111  0.77777778]]


In order to get the best matches possible both here and below in 2-b, I chose to use small weights (*.5*) for any features that went in a 'diagonal' direction and containes both 'x' and 'y' components.  I used a straight weight of *1* for most of the components that contained only 'x' or 'y' gradients. However, my large change was that I used a weight of *100* for the element 'nu30' (moment for x^3) for the MEI passed in to the moment function. This allowed me much greater accuracy by seperating out the images into 'buckets' based on the total area of the motion image in the 'x' direction, which I noticed varied greatly between the three movements. This change alone greatly increased my accuracy, especially in question 2-b.

I also greatly increased the number or 'opening' operation iteration that I performed in order to clean up as much 'false-positive' movement as I could, and carefully selected the start and end point for each motion to capture exactly the movement that I wanted to match againt with as little extraneous movements as possible, especially in the head and torso region.

Note: I did not use different weights or images between 2-a and 2-b.


2-b:

Confusion matrix for P1:-

    [[ 1.          0.          0.        ]
    [ 0.          1.          0.        ]
    [ 0.          0.33333333  0.66666667]]


Confusion matrix for P2:-

    [[ 1.          0.          0.        ]
    [ 0.          1.          0.        ]
    [ 0.          0.33333333  0.66666667]]


Confusion matrix for P3:-

    [[ 1.  0.  0.]
    [ 0.  1.  0.]
    [ 0.  0.  1.]]


Average confusion matrix:-

    [[ 1.          0.          0.        ]
    [ 0.          1.          0.        ]
    [ 0.          0.22222222  0.77777778]]

(Note: Copied text from 2-a to aid grading. My strategy and weights were the same for both questions)

In order to get the best matches possible both here and above in 2-a, I chose to use small weights (*.5*) for any features that went in a 'diagonal' direction and containes both 'x' and 'y' components.  I used a straight weight of *1* for most of the components that contained only 'x' or 'y' gradients. However, my large change was that I used a weight of *100* for the element 'nu30' (moment for x^3) for the MEI passed in to the moment function. This allowed me much greater accuracy by seperating out the images into 'buckets' based on the total area of the motion image in the 'x' direction, which I noticed varied greatly between the three movements. This change alone greatly increased my accuracy.

I also greatly increased the number or 'opening' operation iteration that I performed in order to clean up as much 'false-positive' movement as I could, and carefully selected the start and end point for each motion to capture exactly the movement that I wanted to match againt with as little extraneous movements as possible, especially in the head and torso region.

Note: I did not use different weights or images between 2-a and 2-b.



[ps8-1-a-1]: ./output/ps8-1-a-1.png  "ps8-1-a-1"
[ps8-1-a-2]: ./output/ps8-1-a-2.png  "ps8-1-a-2"
[ps8-1-a-3]: ./output/ps8-1-a-3.png  "ps8-1-a-3"
[ps8-1-b-1]: ./output/ps8-1-b-1.png  "ps8-1-b-1"
[ps8-1-b-2]: ./output/ps8-1-b-2.png  "ps8-1-b-2"
[ps8-1-b-3]: ./output/ps8-1-b-3.png  "ps8-1-b-3"