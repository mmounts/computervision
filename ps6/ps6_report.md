Mitchell Mounts

GA ID: 903176000

Computer Vision Fall 2015 OMSCS

PS6 Report

**1:**

1-a:


ps6-1-a-1.png
![ps6-1-a-1]

ps6-1-a-2.png
![ps6-1-a-2]

Text Response:
I blurred the images for this problem with a Gaussion 49x49 kernel.
This primarily helped with the first two images with the smaller movement, but also
made the results of the final image (1-b3) slightly more accurate.

1-b:

ps6-1-b-1.png
![ps6-1-b-1]

ps6-1-b-2.png
![ps6-1-b-2]

ps6-1-b-3.png
![ps6-1-b-3]

Text Response:
The first two images (shifted by 10 and 20) were somewhat accurate when compared to the
R2 shift image in 1-a-1. However, the tight middle grouping had larger and larger holes
the more that the image shifted, thanks to the inability of the single-pass LK method
to spot large movements between single frames.  Only small areas of the correct displacement
were located between the original and the 40 pixel shifted image.

**2:**

2-a:

ps6-2-a-1.png
![ps6-2-a-1]

2-b:

ps6-2-b-1.png
![ps6-2-b-1]

**3:**

3-a:

ps6-3-a-1.png
![ps6-3-a-1]

ps6-3-a-2.png
![ps6-3-a-2]

ps6-3-a-3.png
![ps6-3-a-3]

ps6-3-a-4.png
![ps6-3-a-4]

**4:**

4-a:

ps6-4-a-1.png
![ps6-4-a-1]

ps6-4-a-2.png
![ps6-4-a-2]

Warped R10 -> 0
![ps6-4-a-test1]

Warped R20 -> 0
![ps6-4-a-test2]


Warped R40 -> 0
![ps6-4-a-test3]


4-b:

ps6-4-b-1.png
![ps6-4-b-1]

ps6-4-b-2.png
![ps6-4-b-2]


Warped yos_img_02 -> yos_img_01
![ps6-4-b-test1]

Warped yos_img_01 -> yos_img_02
![ps6-4-b-test2]

4-c:

ps6-4-c-1.png
![ps6-4-c-1]

ps6-4-c-2.png
![ps6-4-c-2]


Warped data_img_1 -> data_img_0
![ps6-4-c-test1]

Warped data_img_2 -> data_img_1
![ps6-4-c-test2]


**5:**

5-a:

ps6-5-a-1.png
![ps6-5-a-1]

ps6-5-a-2.png
![ps6-5-a-2]


Warped juggle_1 -> juggle_0
![ps6-5-a-test1]

Warped juggle_2 -> juggle_1
![ps6-5-a-test2]


[ps6-1-a-1]: ./output/ps6-1-a-1.png  "ps6-1-a-1"
[ps6-1-a-2]: ./output/ps6-1-a-2.png  "ps6-1-a-1"
[ps6-1-b-1]: ./output/ps6-1-b-1.png  "ps6-1-b-1"
[ps6-1-b-2]: ./output/ps6-1-b-2.png  "ps6-1-b-2"
[ps6-1-b-3]: ./output/ps6-1-b-3.png  "ps6-1-b-3"
[ps6-2-a-1]: ./output/ps6-2-a-1.png  "ps6-2-a-1"
[ps6-2-b-1]: ./output/ps6-2-b-1.png  "ps6-2-b-1"
[ps6-3-a-1]: ./output/ps6-3-a-1.png  "ps6-3-a-1"
[ps6-3-a-2]: ./output/ps6-3-a-2.png  "ps6-3-a-2"
[ps6-3-a-3]: ./output/ps6-3-a-3.png  "ps6-3-a-3"
[ps6-3-a-4]: ./output/ps6-3-a-4.png  "ps6-3-a-4"
[ps6-4-a-1]: ./output/ps6-4-a-1.png  "ps6-4-a-1"
[ps6-4-a-2]: ./output/ps6-4-a-2.png  "ps6-4-b-2"
[ps6-4-a-test1]: ./output/ps6-4-a-test1.png  "ps6-4-a-test1"
[ps6-4-a-test2]: ./output/ps6-4-a-test2.png  "ps6-4-a-test2"
[ps6-4-a-test3]: ./output/ps6-4-a-test3.png  "ps6-4-a-test3"
[ps6-4-b-1]: ./output/ps6-4-b-1.png  "ps6-4-a-1"
[ps6-4-b-2]: ./output/ps6-4-b-2.png  "ps6-4-b-2"
[ps6-4-b-test1]: ./output/ps6-4-b-test1.png  "ps6-4-b-test1"
[ps6-4-b-test2]: ./output/ps6-4-b-test2.png  "ps6-4-b-test2"
[ps6-4-c-1]: ./output/ps6-4-c-1.png  "ps6-4-a-1"
[ps6-4-c-2]: ./output/ps6-4-c-2.png  "ps6-4-b-2"
[ps6-4-c-test1]: ./output/ps6-4-c-test1.png  "ps6-4-c-test1"
[ps6-4-c-test2]: ./output/ps6-4-c-test2.png  "ps6-4-c-test2"
[ps6-5-a-1]: ./output/ps6-5-a-1.png  "ps6-5-a-1"
[ps6-5-a-2]: ./output/ps6-5-a-2.png  "ps6-5-a-2"
[ps6-5-a-test1]: ./output/ps6-5-a-test1.png  "ps6-5-a-test1"
[ps6-5-a-test2]: ./output/ps6-5-a-test2.png  "ps6-5-a-test2"