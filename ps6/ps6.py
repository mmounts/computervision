"""Problem Set 6: Optic Flow."""

import numpy as np
import cv2

import os

# I/O directories
input_dir = "input"
output_dir = "output"


# Assignment code
def optic_flow_LK(A, B):
    """Compute optic flow using the Lucas-Kanade method.

    Parameters
    ----------
        A: grayscale floating-point image, values in [0.0, 1.0]
        B: grayscale floating-point image, values in [0.0, 1.0]

    Returns
    -------
        U: raw displacement (in pixels) along X-axis, same size as image, floating-point type
        V: raw displacement (in pixels) along Y-axis, same size and type as U
    """
    kernel_size = 11;
    U = np.zeros(A.shape); V = np.zeros(A.shape)
    # A = cv2.copyMakeBorder(A, kernel_size, kernel_size, kernel_size, kernel_size, cv2.BORDER_CONSTANT)
    # B = cv2.copyMakeBorder(B, kernel_size, kernel_size, kernel_size, kernel_size, cv2.BORDER_CONSTANT)
    # print U.shape, A.shape

    # A = cv2.GaussianBlur(A, (blur, blur), 5); B = cv2.GaussianBlur(B, (blur, blur), 5)
    sum_kernel = np.ones([kernel_size, kernel_size])
    Ix = cv2.Sobel(A, cv2.CV_64F, 1, 0, ksize=3)
    # Ix = cv2.normalize(Ix, Ix, alpha=-1.0, beta=1.0, norm_type=cv2.NORM_MINMAX)
    Iy = cv2.Sobel(A, cv2.CV_64F, 0, 1, ksize=3)
    # Iy = cv2.normalize(Iy, Iy, alpha=-1.0, beta=1.0, norm_type=cv2.NORM_MINMAX)
    It = A - B

    Ixx = cv2.filter2D(Ix*Ix, -1, sum_kernel, borderType=cv2.BORDER_CONSTANT)
    Iyy = cv2.filter2D(Iy*Iy, -1, sum_kernel, borderType=cv2.BORDER_CONSTANT)
    Ixy = cv2.filter2D(Ix*Iy, -1, sum_kernel, borderType=cv2.BORDER_CONSTANT)
    Ixt = cv2.filter2D(Ix*It, -1, sum_kernel, borderType=cv2.BORDER_CONSTANT)
    Iyt = cv2.filter2D(Iy*It, -1, sum_kernel, borderType=cv2.BORDER_CONSTANT)

    for x in range(U.shape[0]):
        for y in range(U.shape[1]):
            # print a
            # A_sq = np.dot(np.transpose(a), a)
            A_sq = [[Ixx[x][y], Ixy[x][y]],[Ixy[x][y], Iyy[x][y]]];
            B_sq = [[Ixt[x][y]],[Iyt[x][y]]]
            # B_sq = np.dot(np.transpose(a), b)
            # u_v, error, rank, sing = np.linalg.lstsq(A_sq, B_sq)
            u_v = np.dot(np.linalg.inv(A_sq), B_sq)
            # print u_v
            U[x][y] = u_v[0] if abs(u_v[0]) > .0001 else 0
            V[x][y] = u_v[1] if abs(u_v[1]) > .0001 else 0
            # U[x][y] = u_v[0]; V[x][y] = u_v[1]
            # print U[x][y], x, y

    # print U
    return U, V


def reduce(image):
    """Reduce image to the next smaller level.

    Parameters
    ----------
        image: grayscale floating-point image, values in [0.0, 1.0]

    Returns
    -------
        reduced_image: same type as image, half size
    """
    half_x = image.shape[0] / 2; half_y = image.shape[1] / 2
    reduced_image = np.zeros([half_x + (half_x % 2), half_y + (half_y % 2)])
    single_filter = np.array([[1.0/16, 1.0/4, 3.0/8, 1.0/4, 1.0/16]])
    sum_kernel = np.dot(np.transpose(single_filter), single_filter)

    filtered_img = cv2.filter2D(image, -1, sum_kernel, borderType=cv2.BORDER_CONSTANT)

    for x in range(0, image.shape[0], 2):
        for y in range(0, image.shape[1], 2):
            # / 25 to get average of sum
            if x/2 < reduced_image.shape[0] and y/2 < reduced_image.shape[1]:
                reduced_image[x/2][y/2] = filtered_img[x][y]
    # print image.shape, reduced_image.shape
    return reduced_image


def gaussian_pyramid(image, levels):
    """Create a Gaussian pyramid of given image.

    Parameters
    ----------
        image: grayscale floating-point image, values in [0.0, 1.0]
        levels: number of levels in the resulting pyramid

    Returns
    -------
        g_pyr: Gaussian pyramid, with g_pyr[0] = image
    """
    g_pyr = [image]
    for lvl in range(1, levels):
        g_pyr.append(reduce(g_pyr[lvl-1]))

    # print g_pyr[0]
    return g_pyr


def expand(image):
    """Expand image to the next larger level.

    Parameters
    ----------
        image: grayscale floating-point image, values in [0.0, 1.0]

    Returns
    -------
        reduced_image: same type as image, double size
    """
    expanded_image = np.zeros([image.shape[0] * 2, image.shape[1] * 2])
    # single_filter = np.array([[1.0/16, 1.0/4, 3.0/8, 1.0/4, 1.0/16]])
    # sum_kernel = np.dot(np.transpose(single_filter), single_filter)
    even_filter = np.array([[1.0/8, 3.0/4, 1.0/8]])
    # odd_filter = np.array([[1/2, 0, 1/2]])

    even_kernel = np.dot(np.transpose(even_filter), even_filter)
    # odd_kernel = np.dot(np.transpose(odd_filter), odd_filter)
    even_img = cv2.filter2D(image, -1, even_kernel, borderType=cv2.BORDER_CONSTANT)
    # odd_img = cv2.filter2D(image, -1, odd_kernel, borderType=cv2.BORDER_CONSTANT)

    # even_img
    # print image.shape, expanded_image.shape
    for x in range(expanded_image.shape[0]):
        for y in range(expanded_image.shape[1]):
            if x % 2 == 1 and y % 2 == 1:
                expanded_image[x][y] = even_img[x/2][y/2]
                # expanded_image[x][y] = 0
                # print '1: ', expanded_image[x][y]
            elif x/2 < image.shape[0] - 1 and y/2 < image.shape[1] - 1:
                # expanded_image[x][y] = 0
                expanded_image[x][y] = (image[x/2][y/2] + image[(x/2)+1][y/2] + image[x/2][(y/2)+1] + image[(x/2)+1][(y/2)+1]) * .25
                # print '2: ', expanded_image[x][y]

    return expanded_image


def laplacian_pyramid(g_pyr):
    """Create a Laplacian pyramid from a given Gaussian pyramid.

    Parameters
    ----------
        g_pyr: Gaussian pyramid, as returned by gaussian_pyramid()

    Returns
    -------
        l_pyr: Laplacian pyramid, with l_pyr[-1] = g_pyr[-1]
    """

    # TODO: Your code here
    l_pyr = [g_pyr[-1]]
    for l in range(-1 , -len(g_pyr), -1):
        # print l
        expanded_image = expand(g_pyr[l])
        # print expanded_image.shape, g_pyr[l-1].shape
        if g_pyr[l-1].shape != expanded_image.shape:
            expanded_image = expanded_image[:-2, :-2]
        l_pyr.insert(0, g_pyr[l-1] - expanded_image)
        # l_pyr.insert(0, expanded_image)

    return l_pyr


def warp(image, U, V):
    """Warp image using X and Y displacements (U and V).

    Parameters
    ----------
        image: grayscale floating-point image, values in [0.0, 1.0]

    Returns
    -------
        warped: warped image, such that warped[y, x] = image[y + V[y, x], x + U[y, x]]

    """
    warped = image.copy()
    for x in range(image.shape[0]):
        for y in range(image.shape[1]):
            # print x+V[x, y], y+U[x,y], U.shape, V.shape, image.shape
            if 0 < x+V[x, y] < image.shape[0] and 0 < y + U[x,y] < image.shape[1]:
                warped[x][y] = image[x + V[x, y]][y + U[x, y]]

    return warped


def hierarchical_LK(A, B, levels=4):
    """Compute optic flow using the Hierarchical Lucas-Kanade method.

    Parameters
    ----------
        A: grayscale floating-point image, values in [0.0, 1.0]
        B: grayscale floating-point image, values in [0.0, 1.0]

    Returns
    -------
        U: raw displacement (in pixels) along X-axis, same size as image, floating-point type
        V: raw displacement (in pixels) along Y-axis, same size and type as U
    """
    k = levels
    a_gpyr = gaussian_pyramid(A, k)
    b_gpyr = gaussian_pyramid(B, k)
    U = np.zeros(a_gpyr[k-1].shape)
    V = np.zeros(b_gpyr[k-1].shape)
    k -= 1

    while k > 0:
        Ck = warp(b_gpyr[k], U, V)
        Dx, Dy = optic_flow_LK(a_gpyr[k], Ck)

        if U.shape != Dx.shape:
            Dx = cv2.copyMakeBorder(Dx, 2, 0, 2, 0, cv2.BORDER_CONSTANT, value=0)

        if V.shape != Dy.shape:
            Dy = cv2.copyMakeBorder(Dy, 2, 0, 2, 0, cv2.BORDER_CONSTANT, value=0)
        # print 'U: ', U
        # print 'V: ', V
        # print 'Dx: ', Dx
        # print 'Dy: ', Dy
        # print ''
        U = U + Dx; V = V + Dy
        k -= 1
        U = 2 * expand(U); V = 2 * expand(V)

    U = U * 10; V = V * 10
    return U, V

def make_image_pair(image1, image2):
    """Adjoin two images side-by-side to make a single new image.

    Parameters
    ----------
        image1: first image, could be grayscale or color (BGR)
        image2: second image, same type as first

    Returns
    -------
        image_pair: combination of both images, side-by-side, same type
    """

    # if image1.shape != image2.shape:
        # print image1.shape, image2.shape
    image_pair = np.concatenate((image1, image2), axis=1)
    return image_pair

def stack_image_pair(image1, image2):
    image_pair = np.concatenate((image1, image2), axis=0)
    return image_pair

def print_image(image, output_file):
    image_out = image.copy()
    image_out = cv2.normalize(image_out, image_out, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    cv2.imwrite(os.path.join(output_dir, output_file), image_out)

def print_color_map(image, output_file):
    image_out = image.copy()
    # print image_out
    image_out = cv2.normalize(image_out, image_out, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # print image_out
    image_out = cv2.applyColorMap(image_out, cv2.COLORMAP_JET)
    # print image_out
    if output_file:
        cv2.imwrite(os.path.join(output_dir, output_file), image_out)

    return image_out

def make_image_pyramid(images):
    pyramid = images[0].copy()
    height = pyramid.shape[0]
    for idx in range(1, len(images)):
        # print images[idx].shape
        cur_image = images[idx]
        cur_image = cv2.copyMakeBorder(cur_image, 0, height - cur_image.shape[0], 0, 0, cv2.BORDER_CONSTANT)
        # print height, cur_image.shape
        pyramid = make_image_pair(pyramid, cur_image)
    return pyramid


# Driver code
def main():
    # Note: Comment out parts of this code as necessary
    # np.set_printoptions(threshold=10)

    # 1a
    Shift0 = cv2.imread(os.path.join(input_dir, 'TestSeq', 'Shift0.png'), 0) / 255.0
    ShiftR2 = cv2.imread(os.path.join(input_dir, 'TestSeq', 'ShiftR2.png'), 0) / 255.0
    ShiftR5U5 = cv2.imread(os.path.join(input_dir, 'TestSeq', 'ShiftR5U5.png'), 0) / 255.0
    ShiftR10 = cv2.imread(os.path.join(input_dir, 'TestSeq', 'ShiftR10.png'), 0) / 255.0
    ShiftR20 = cv2.imread(os.path.join(input_dir, 'TestSeq', 'ShiftR20.png'), 0) / 255.0
    ShiftR40 = cv2.imread(os.path.join(input_dir, 'TestSeq', 'ShiftR40.png'), 0) / 255.0
    # Optionally, smooth the images if LK doesn't work well on raw images
    U, V = optic_flow_LK(Shift0, ShiftR2)
    print_color_map(make_image_pair(U, V), 'ps6-1-a-1.png')

    # Similarly for Shift0 and ShiftR5U5
    U, V = optic_flow_LK(Shift0, ShiftR5U5)
    print_color_map(make_image_pair(U, V), 'ps6-1-a-2.png')

    # 1b
    # TODO: Similarly for ShiftR10, ShiftR20 and ShiftR40
    U, V = optic_flow_LK(Shift0, ShiftR10)
    print_color_map(make_image_pair(U, V), 'ps6-1-b-1.png')

    U, V = optic_flow_LK(Shift0, ShiftR20)
    print_color_map(make_image_pair(U, V), 'ps6-1-b-2.png')

    U, V = optic_flow_LK(Shift0, ShiftR40)
    print_color_map(make_image_pair(U, V), 'ps6-1-b-3.png')

    # # 2a
    # yos_img_01 = cv2.imread(os.path.join(input_dir, 'DataSeq1', 'yos_img_01.jpg'), 0) / 255.0
    # yos_img_01_g_pyr = gaussian_pyramid(yos_img_01, 4)  # TODO: implement this
    # # # Save pyramid images as a single side-by-side image (write a utility function?)
    # pyramid = make_image_pyramid(yos_img_01_g_pyr)
    # pyramid = cv2.normalize(pyramid, pyramid, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    # cv2.imwrite(os.path.join(output_dir, 'ps6-2-a-1.png'), pyramid)

    # # # 2b
    # yos_img_01_l_pyr = laplacian_pyramid(yos_img_01_g_pyr)
    # # # Save pyramid images as a single side-by-side image
    # pyramid = make_image_pyramid(yos_img_01_l_pyr)
    # pyramid = cv2.normalize(pyramid, pyramid, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    # cv2.imwrite(os.path.join(output_dir, 'ps6-2-b-1.png'), pyramid)

    # # 3a
    # yos_img_02 = cv2.imread(os.path.join(input_dir, 'DataSeq1', 'yos_img_02.jpg'), 0) / 255.0
    # yos_img_02_g_pyr = gaussian_pyramid(yos_img_02, 4)
    # # Select appropriate pyramid *level* that leads to best optic flow estimation
    # level = 2
    # U, V = optic_flow_LK(yos_img_01_g_pyr[level], yos_img_02_g_pyr[level])

    # # Scale up U, V to original image size (note: don't forget to scale values as well!)
    # while level > 0:
    #     U = expand(U) * 2; V = expand(V) * 2
    #     level -= 1

    # # Save U, V as side-by-side false-color image or single quiver plot
    # print_color_map(make_image_pair(U, V), 'ps6-3-a-1.png')

    # yos_img_02_warped = warp(yos_img_02, U, V)
    # # Save difference image between yos_img_02_warped and original yos_img_01
    # yos_diff = yos_img_02_warped - yos_img_01
    # # Note: Scale values such that zero difference maps to neutral gray, max -ve to black and max +ve to white
    # yos_diff = cv2.normalize(yos_diff, yos_diff, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # cv2.imwrite(os.path.join(output_dir, 'ps6-3-a-2.png'), yos_diff)

    # # Similarly, you can compute displacements for yos_img_02 and yos_img_03 (but no need to save images)
    # yos_img_03 = cv2.imread(os.path.join(input_dir, 'DataSeq1', 'yos_img_03.jpg'), 0) / 255.0

    # # Repeat for DataSeq2 (save images)
    # data_img_0 = cv2.imread(os.path.join(input_dir, 'DataSeq2', '0.png'), 0) / 255.0
    # data_img_1 = cv2.imread(os.path.join(input_dir, 'DataSeq2', '1.png'), 0) / 255.0
    # data_img_2 = cv2.imread(os.path.join(input_dir, 'DataSeq2', '2.png'), 0) / 255.0
    # data_img_0_g_pyr = gaussian_pyramid(data_img_0, 4)
    # data_img_1_g_pyr = gaussian_pyramid(data_img_1, 4)

    # level = 1
    # U, V = optic_flow_LK(data_img_0_g_pyr[level], data_img_1_g_pyr[level])

    #  # Scale up U, V to original image size (note: don't forget to scale values as well!)
    # while level > 0:
    #     U = expand(U) * 2; V = expand(V) * 2
    #     level -= 1

    # # Save U, V as side-by-side false-color image or single quiver plot
    # print_color_map(make_image_pair(U, V), 'ps6-3-a-3.png')

    # data_1_warped = warp(data_img_1, U, V)

    # data_diff = data_1_warped - data_img_0
    # data_diff = cv2.normalize(data_diff, data_diff, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # cv2.imwrite(os.path.join(output_dir, 'ps6-3-a-4.png'), data_diff)

    # # 4a
    # U10, V10 = hierarchical_LK(Shift0, ShiftR10)  # TODO: implement this
    # U20, V20 = hierarchical_LK(Shift0, ShiftR20)
    # U40, V40 = hierarchical_LK(Shift0, ShiftR40)
    # # Save displacement image pairs (U, V), stacked
    # # Hint: You can use np.concatenate()
    # pair_10 = make_image_pair(U10, V10)
    # pair_10_p = print_color_map(pair_10, None)
    # pair_20 = make_image_pair(U20, V20)
    # pair_20_p = print_color_map(pair_20, None)
    # pair_40 = make_image_pair(U40, V40)
    # pair_40_p = print_color_map(pair_40, None)
    # stacked = stack_image_pair(pair_10_p, pair_20_p)
    # stacked = stack_image_pair(stacked, pair_40_p)
    # print_color_map(stacked, 'ps6-4-a-1.png')
    # stacked = cv2.normalize(stacked, stacked, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # cv2.imwrite(os.path.join(output_dir, 'ps6-4-a-1.png'), stacked)

    # # print U10, V10
    # ShiftR10_warped = warp(ShiftR10, U10, V10)
    # # print U20, V20
    # ShiftR20_warped = warp(ShiftR20, U20, V20)
    # # print U40, V40
    # ShiftR40_warped = warp(ShiftR40, U40, V40)
    # # print_image(make_image_pair(Shift0, ShiftR10_warped), 'ps6-4-a-test1.png')
    # # print_image(make_image_pair(Shift0, ShiftR20_warped), 'ps6-4-a-test2.png')
    # # print_image(make_image_pair(Shift0, ShiftR40_warped), 'ps6-4-a-test3.png')

    # # Save difference between each warped image and original image (Shift0), stacked
    # diff_10 = ShiftR10_warped - Shift0; diff_10 = cv2.normalize(diff_10, diff_10, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # diff_20 = ShiftR20_warped - Shift0; diff_20 = cv2.normalize(diff_20, diff_20, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # diff_40 = ShiftR40_warped - Shift0; diff_40 = cv2.normalize(diff_40, diff_40, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # stacked = stack_image_pair(diff_10, diff_20)
    # stacked = stack_image_pair(stacked, diff_40)

    # cv2.imwrite(os.path.join(output_dir, 'ps6-4-a-2.png'), stacked)

    # # 4b
    # # Repeat for DataSeq1 (use yos_img_01.png as the original)
    # U1, V1 = hierarchical_LK(yos_img_01, yos_img_02, 2)
    # U2, V2 = hierarchical_LK(yos_img_02, yos_img_03, 2)
    # pair_1 = make_image_pair(U1, V1)
    # pair_1_p = print_color_map(pair_1, None)
    # pair_2 = make_image_pair(U2, V2)
    # pair_2_p = print_color_map(pair_2, None)
    # stacked = stack_image_pair(pair_1_p, pair_2_p)
    # cv2.imwrite(os.path.join(output_dir, 'ps6-4-b-1.png'), stacked)

    # yos_warp_1 = warp(yos_img_02, U1, V1)
    # yos_warp_2 = warp(yos_img_03, U2, V2)
    # # print_image(make_image_pair(yos_img_01, yos_warp_1), 'ps6-4-b-test1.png')
    # # print_image(make_image_pair(yos_img_02, yos_warp_2), 'ps6-4-b-test2.png')

    # diff_1 = yos_warp_1 - yos_img_01; diff_1 =  cv2.normalize(diff_1, diff_1, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # diff_2 = yos_warp_2 - yos_img_01; diff_2 =  cv2.normalize(diff_2, diff_2, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # stacked = stack_image_pair(diff_1, diff_2)

    # cv2.imwrite(os.path.join(output_dir, 'ps6-4-b-2.png'), stacked)

    # # # 4c
    # # # Repeat for DataSeq1 (use 0.png as the original)    
    # U1, V1 = hierarchical_LK(data_img_0, data_img_1, 2)
    # U2, V2 = hierarchical_LK(data_img_0, data_img_2, 2)
    # pair_1 = make_image_pair(U1, V1)
    # pair_1_p = print_color_map(pair_1, None)
    # pair_2 = make_image_pair(U2, V2)
    # pair_2_p = print_color_map(pair_2, None)
    # stacked = stack_image_pair(pair_1_p, pair_2_p)
    # cv2.imwrite(os.path.join(output_dir, 'ps6-4-c-1.png'), stacked)

    # data_warp_1 = warp(data_img_1, U1, V1)
    # data_warp_2 = warp(data_img_2, U2, V2)
    # # print_image(make_image_pair(data_img_0, data_warp_1), 'ps6-4-c-test1.png')
    # # print_image(make_image_pair(data_img_1, data_warp_2), 'ps6-4-c-test2.png')

    # diff_1 = data_warp_1 - data_img_0; diff_1 =  cv2.normalize(diff_1, diff_1, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # diff_2 = data_warp_1 - data_img_0; diff_2 =  cv2.normalize(diff_2, diff_2, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # stacked = stack_image_pair(diff_1, diff_2)

    # cv2.imwrite(os.path.join(output_dir, 'ps6-4-c-2.png'), stacked)

    # # 5
    # # Apply to Juggle sequence
    # juggle_0 = cv2.imread(os.path.join(input_dir, 'Juggle', '0.png'), 0) / 255.0
    # juggle_1 = cv2.imread(os.path.join(input_dir, 'Juggle', '1.png'), 0) / 255.0
    # juggle_2 = cv2.imread(os.path.join(input_dir, 'Juggle', '2.png'), 0) / 255.0 
    # U1, V1 = hierarchical_LK(juggle_0, juggle_1)
    # U2, V2 = hierarchical_LK(juggle_0, juggle_2)
    # pair_1 = make_image_pair(U1, V1)
    # pair_1_p = print_color_map(pair_1, None)
    # pair_2 = make_image_pair(U2, V2)
    # pair_2_p = print_color_map(pair_2, None)
    # stacked = stack_image_pair(pair_1_p, pair_2_p)
    # cv2.imwrite(os.path.join(output_dir, 'ps6-5-a-1.png'), stacked)

    # juggle_warp_1 = warp(juggle_0, U1, V1)
    # juggle_warp_2 = warp(juggle_1, U2, V2)
    # # print_image(make_image_pair(juggle_0, juggle_warp_1), 'ps6-5-a-test1.png')
    # # print_image(make_image_pair(juggle_1, juggle_warp_2), 'ps6-5-a-test2.png')

    # diff_1 = juggle_warp_1 - juggle_0; diff_1 =  cv2.normalize(diff_1, diff_1, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # diff_2 = juggle_warp_1 - juggle_0; diff_2 =  cv2.normalize(diff_2, diff_2, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)
    # stacked = stack_image_pair(diff_1, diff_2)

    # cv2.imwrite(os.path.join(output_dir, 'ps6-5-a-2.png'), stacked) 


if __name__ == "__main__":
    main()
