"""Problem Set 2: Edges and Lines."""

import numpy as np
import cv2

import os
from math import pi, hypot, ceil, cos, sin, degrees

input_dir = "input"  # read images from os.path.join(input_dir, <filename>)
output_dir = "output"  # write images to os.path.join(output_dir, <filename>)
points = None

def hough_lines_acc(img_edges, rho_res=1, theta_res=pi/90, smooth=True, sobelX = None, sobelY = None):
    """Compute Hough Transform for lines on edge image.

    Parameters
    ----------
        img_edges: binary edge image
        rho_res: rho resolution (in pixels)
        theta_res: theta resolution (in radians)

    Returns
    -------
        H: Hough accumulator array
        rho: vector of rho values, one for each row of H
        theta: vector of theta values, one for each column of H
        points: array of points that voted for a H bucket
    """
    # Get diagonal length of image (0,0) -> bottom right edge
    diag_len = ceil(hypot(img_edges.shape[0], img_edges.shape[1]))
    # Create empty rho array
    # rho = np.fromfunction(lambda i: i * rho_res, int(ceil(diag_len/rho_res)));
    rho = np.zeros(ceil(diag_len/rho_res), dtype=float)
    for x in range(len(rho)):
        rho[x] = x * rho_res;
    # print rho
    # Create empty theta array
    theta = np.zeros(2 * ceil((pi/2) / theta_res) + 2, dtype=float)
    idx = 0;
    for x in range(-len(theta)/2, (len(theta)/2)):
        theta[idx] = x * theta_res
        idx += 1

    # Create H accumulator array
    H = np.zeros([rho.size, theta.size], dtype=int)
    global points
    points = []
    for x in range(H.shape[0]):
        points.append([])
        for y in range(H.shape[1]):
            points[x].append([])
    # print range(-len(theta)/2, len(theta)/2), theta

    # Run Hough transfor algorithm
    for x in range(img_edges.shape[1]):
        for y in range(img_edges.shape[0]):
            if img_edges[y, x] > 0:
                theta_range = [-361, 361]
                if sobelX is not None and sobelY is not None:
                    est_theta = np.arctan2(sobelY[y, x], sobelX[y, x])
                    theta_range = [int(ceil(degrees(est_theta - pi/4))), int(ceil(degrees(est_theta + pi/4)))]

                for theta_index in range(len(theta)):
                    theta_val = theta[theta_index]
                    if degrees(theta_val) >= theta_range[0] and degrees(theta_val) <= theta_range[1]:
                        d_val = x * cos(theta_val) + y * sin(theta_val)
                        d_val = abs(d_val - (d_val % rho_res))
                        d_index = np.where(rho == d_val)[0]

                        H[d_index, theta_index] += 1
                        if [y,x] not in points[d_index][theta_index]: points[d_index][theta_index].append([y, x])

                        if smooth:
                            for small_x in range(d_index-1, d_index+1):
                                for small_y in range(theta_index-1, theta_index+1):
                                    if small_x > 0 and small_x < len(rho) and small_y > 0 and small_y < len(theta):
                                        H[small_x, small_y] += 1
                                        if [y,x] not in points[small_x][small_y]: points[small_x][small_y].append([y, x])

    return H, rho, theta


def hough_peaks(H, Q, threshold=0, neighborhood=1, thetas=None):
    """Find peaks (local maxima) in accumulator array.

    Parameters
    ----------
        H: Hough accumulator array
        Q: number of peaks to find (max)

    Returns
    -------
        peaks: Px2 matrix (P <= Q) where each row is a (rho_idx, theta_idx) pair
    """
    max_x = H.shape[0]
    max_y = H.shape[1]

    def is_local_maxima(x, y):
        checking_val = H[x,y]
        for i in range(-neighborhood, neighborhood):
            for j in range(-neighborhood, neighborhood):
                if (i+j) > 0 and 0 < (x + i) < max_x and 0 < (y + j) < max_y:
                    if H[x+i, y+j] > checking_val:
                        return False
        return True

    def blank_neighborhood(x, y):
        for i in range(-neighborhood, neighborhood):
            for j in range(-neighborhood, neighborhood):
                if (i+j) > 0 and 0 < (x + i) < max_x and 0 < (y + j) < max_y:
                    H[x+i, y+j] = 0


    peaks = []

    for x in range(max_x):
        for y in range(max_y):
            if H[x, y] > threshold and is_local_maxima(x, y):
                peaks.append([H[x, y], x, y])
                blank_neighborhood(x, y)

    if thetas is not None:
        peaks = find_parallel_peaks(peaks, thetas, max_distance=30)

    if len(peaks) > Q:
        peaks = sorted(peaks, key=lambda peak: peak[0])[len(peaks) - Q:]
    peaks = np.array(peaks)
    # print peaks

    return peaks[:,1:]


def hough_lines_draw(img_out, peaks, rho, theta):
    """Draw lines on an image corresponding to accumulator peaks.

    Parameters
    ----------
        img_out: 3-channel (color) image
        peaks: Px2 matrix where each row is a (rho_idx, theta_idx) index pair
        rho: vector of rho values, such that rho[rho_idx] is a valid rho value
        theta: vector of theta values, such that theta[theta_idx] is a valid theta value
    """
    x_len = int(img_out.shape[0])
    y_len = int(img_out.shape[1])

    def draw_line(rho0, theta0):
        a = np.cos(theta0)
        b = np.sin(theta0)
        x0 = a*rho0
        y0 = b*rho0
        x1 = int(x0 + 1000*(-b))
        y1 = int(y0 + 1000*(a))
        x2 = int(x0 - 1000*(-b))
        y2 = int(y0 - 1000*(a))
        pt1 = (x1, y1); pt2 = (x2, y2)

        cv2.line(img_out, pt1, pt2, (0,125,255), 4)

    for peak in peaks:
        draw_line(rho[peak[0]], theta[peak[1]])


def hough_circles_acc(img_edges, radius=20, dim_step = 1, smooth=True, sobelX=None, sobelY=None):
    """Compute Hough array for circles of a given radius on edge image.

    Parameters
    ----------
        img_edges: binary edge image
        radius: radius of circles to compute

    Returns
    -------
        H: Hough accumulator array (same size as image)
    """
    # Create H accumulator array
    H = np.zeros(img_edges.shape, dtype=int)

    # Run Hough accumulator algorithm
    for x in range(0, H.shape[1], dim_step):
        for y in range(0, H.shape[0], dim_step):
            if img_edges[y,x] > 0:
                theta_range = [0, 360]
                # If we passed in gradient info to use as an estimate, use it to get smaller theta range
                if sobelX is not None and sobelY is not None:
                    est_theta = np.arctan2(sobelY[y, x], sobelX[y, x])
                    theta_range = [int(ceil(degrees(est_theta - pi/8))), int(ceil(degrees(est_theta + pi/8)))]

                for theta in range(theta_range[0], theta_range[1]):
                    a = int(x - radius * cos(theta))
                    b = int(y + radius * sin(theta))
                    if a >= 0 and b >= 0 and a < H.shape[0] and b < H.shape[1]:
                        H[a, b] += 1
                        if smooth:
                            for small_x in range(a-1, a+1):
                                for small_y in range(b-1, b+1):
                                    if small_x > 0 and small_x < H.shape[0] and small_y > 0 and small_y < H.shape[1]:
                                        H[small_x, small_y] += 1


    return H


def find_circles(img_edges, r_range=(20, 40), dim_step = 1, peak_min=100, peak_num = 4, sobelX=None, sobelY=None):
    """Compute Hough array for circles of a given radius on edge image.

    Parameters
    ----------
        img_edges: binary edge image
        r_range: range of radii to check

    Returns
    -------
        centers: array of Hough accumulator array (same size as image)
        radii: radius matching the accumulator array of the same index
    """
    centers = []
    radii = []

    for r in range(r_range[0], r_range[1]):
        # Apply Hough methods to smoothed image, tweak parameters to find best lines
        # Note: Write a normalized uint8 version, mapping min value to 0 and max to 255
        h = hough_circles_acc(img_edges, r, dim_step = dim_step, sobelX = sobelX, sobelY = sobelY)
        h = cv2.normalize(h, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
        # print h_4_blur, h_4_blur.shape
        # Find peaks (local maxima) in accumulator arrays
        r_peaks = hough_peaks(h, peak_num, peak_min, 2)
        centers.append(r_peaks)
        radii.append(r)

    return centers, radii

def cut_short_lines(h, h_points, dis_threshold=2, h_threshold=30, length_threshold=20):
    for x in range(h.shape[0]):
        for y in range(h.shape[1]):
            if h[x, y] > h_threshold:
                cur_pts = h_points[x][y]
                total_longest = 0
                cur_longest = 0
                for pt_idx in range(1, len(cur_pts)):
                    # print cur_pts[pt_idx-1], cur_pts[pt_idx], np.linalg.norm(np.array(cur_pts[pt_idx-1]) - np.array(cur_pts[pt_idx]))
                    if np.linalg.norm(np.array(cur_pts[pt_idx-1]) - np.array(cur_pts[pt_idx])) <= dis_threshold:
                        # print cur_pts[pt_idx-1], cur_pts[pt_idx], np.linalg.norm(np.array(cur_pts[pt_idx-1]) - np.array(cur_pts[pt_idx]))
                        cur_longest += 1
                    else:
                        if cur_longest > total_longest: total_longest = cur_longest
                        cur_longest = 0
                # Set line to 'not used' if it is shorter than the threshold
                if total_longest < length_threshold:
                    h[x, y] = 0

    return h

def find_parallel_peaks(peaks, theta, theta_range=1, min_distance=5, max_distance=10):
    """Pick out peaks that have parallel peaks nearby

    Parameters
    ----------
        peaks: array of currently discovered peaks
        theta: array of thetas to match against
        max_distance: max distance to check from each peak for parallels

    Returns
    -------
        new_peaks: new set of peaks, only containing parallel peaks
    """
    new_peaks = []
    for peak1_0 in peaks:
        for peak2_0 in peaks:
            peak1 = peak1_0.tolist()
            peak2 = peak2_0.tolist()
            if (peak1 != peak2 and abs(peak1[1] - peak2[1]) <= theta_range
                and min_distance <= abs(peak1[0] - peak2[0]) <= max_distance
                and peak1 not in new_peaks):
                new_peaks.append(peak1); new_peaks.append(peak2)

    return new_peaks

def ps_1():
    # 1-a
    # Load the input grayscale image
    img = cv2.imread(os.path.join(input_dir, 'ps2-input0.png'), 0)  # flags=0 ensures grayscale

    # Compute edge image (img_edges)
    img_edges = cv2.Canny(img, 100, 200)

    # Write out edge image
    cv2.imwrite(os.path.join(output_dir, 'ps2-1-a-1.png'), img_edges)  # save as ps2-1-a-1.png

def ps_2():
    # 2-a
    # Compute Hough Transform for lines on edge image
    img = cv2.imread(os.path.join(input_dir, 'ps2-input0.png'), 0)  # flags=0 ensures grayscale
    img_edges = cv2.Canny(img, 100, 200)

    H, rho, theta = hough_lines_acc(img_edges, smooth=False)

    # Store accumulator array (H) as ps2-2-a-1.png
    # Note: Write a normalized uint8 version, mapping min value to 0 and max to 255
    H = cv2.normalize(H, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, 'ps2-2-a-1.png'), H)

    # 2-b
    # Find peaks (local maxima) in accumulator array
    peaks = hough_peaks(H, 8, 125, 1) # implement this, try different parameters
    # Random note: neighborhood of 2 kills one of my points

    # Store a copy of accumulator array image (from 2-a), with peaks highlighted, as ps2-2-b-1.png
    h_copy = H.copy()
    for peak in peaks:
        cv2.circle(h_copy, (peak[1], peak[0]), 4, (255, 255, 0), 2)
        # print H[peak[0], peak[1]], points[peak[0]][peak[1]]
    cv2.imwrite(os.path.join(output_dir, 'ps2-2-b-1.png'), h_copy)

    # 2-c
    # Draw lines corresponding to accumulator peaks
    img_out = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)  # copy & convert to color image

    # Draw edge lines onto image
    hough_lines_draw(img_out, peaks, rho, theta)
    cv2.imwrite(os.path.join(output_dir, 'ps2-2-c-1.png'), img_out)  # save as ps2-2-c-1.png

def ps_3():
    # 3
    # 3-a
    # Read ps2-input0-noise.png, compute smoothed image using a Gaussian filter
    img3 = cv2.imread(os.path.join(input_dir, 'ps2-input0-noise.png'), 0)  # flags=0 ensures grayscale
    img3_blur = cv2.GaussianBlur(img3, (5, 5), 5)
    cv2.imwrite(os.path.join(output_dir, 'ps2-3-a-1.png'), img3_blur)  # save as ps2-3-a-1.png

    # 3-b
    # Compute binary edge images for both original image and smoothed version
    img3_edges = cv2.Canny(img3, 100, 200)
    img3_blur_edges = cv2.Canny(img3_blur, 150, 200)
    cv2.imwrite(os.path.join(output_dir, 'ps2-3-b-1.png'), img3_edges)  # save as ps2-3-b-1.png
    cv2.imwrite(os.path.join(output_dir, 'ps2-3-b-2.png'), img3_blur_edges)  # save as ps2-3-b-2.png

    # 3-c
    # Apply Hough methods to smoothed image, tweak parameters to find best lines
    # Store accumulator array (H) as ps2-2-a-1.png
    # Note: Write a normalized uint8 version, mapping min value to 0 and max to 255
    h_3_blur, rho_3_blur, theta_3_blur = hough_lines_acc(img3_blur_edges, smooth=False)
    h_3_blur = cv2.normalize(h_3_blur,  alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    # print h_3_blur, h_3_blur.shape
    # Find peaks (local maxima) in accumulator arrays
    h_3_copy = h_3_blur.copy()
    peaks_3_blur = hough_peaks(h_3_blur, 12, 30, 3)

    # Store a copy of accumulator array image (from 2-a), with peaks highlighted, as ps2-2-b-1.png
    for peak in peaks_3_blur:
        cv2.circle(h_3_copy, (peak[1], peak[0]), 4, (255, 255, 0), 2)

    # Write out peak image 'ps2-3-c-1.png
    cv2.imwrite(os.path.join(output_dir, 'ps2-3-c-1.png'), h_3_copy)

    # Draw edge lines onto image
    img3_out = cv2.cvtColor(img3, cv2.COLOR_GRAY2BGR)  # copy & convert to color image
    hough_lines_draw(img3_out, peaks_3_blur, rho_3_blur, theta_3_blur)
    cv2.imwrite(os.path.join(output_dir, 'ps2-3-c-2.png'), img3_out)  # save as ps2-2-c-1.png

def ps_4():
    # 4
    # 4-a
    # Read ps2-input1.png, compute smoothed image using a Gaussian filter
    img4 = cv2.imread(os.path.join(input_dir, 'ps2-input1.png'), 0)  # flags=0 ensures grayscale
    img4_blur = cv2.GaussianBlur(img4, (3, 3), 5)
    cv2.imwrite(os.path.join(output_dir, 'ps2-4-a-1.png'), img4_blur)  # save as ps2-4-a-1.png

    # 4-b
    # Compute binary edge images for both original image and smoothed version
    img4_blur_edges = cv2.Canny(img4_blur, 350, 400)
    cv2.imwrite(os.path.join(output_dir, 'ps2-4-b-1.png'), img4_blur_edges)  # save as ps2-4-b-1.png

    # 4-c
    # Apply Hough methods to smoothed image, tweak parameters to find best lines
    # Store accumulator array (H) as ps2-2-a-1.png
    # Note: Write a normalized uint8 version, mapping min value to 0 and max to 255
    h_4_blur, rho_4_blur, theta_4_blur = hough_lines_acc(img4_blur_edges, smooth=False)
    h_4_blur = cv2.normalize(h_4_blur, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    # print h_4_blur, h_4_blur.shape
    # Find peaks (local maxima) in accumulator arrays
    peaks_4_blur = hough_peaks(h_4_blur, 8, 75, 2)
    # Min of 100 got 1 rogue lines, min of 125 lost the bottom of top pen
    # To get image, raised canny lower bound to get rid of letter small lines

    # Store a copy of accumulator array image (from 2-a), with peaks highlighted, as ps2-2-b-1.png
    for peak in peaks_4_blur:
        cv2.circle(h_4_blur, (peak[1], peak[0]), 4, (255, 255, 0), 2)

    # Write out peak image 'ps2-4-c-1.png
    cv2.imwrite(os.path.join(output_dir, 'ps2-4-c-1.png'), h_4_blur)

    # Draw edge lines onto image
    img4_out = cv2.cvtColor(img4, cv2.COLOR_GRAY2BGR)  # copy & convert to color image
    hough_lines_draw(img4_out, peaks_4_blur, rho_4_blur, theta_4_blur)
    cv2.imwrite(os.path.join(output_dir, 'ps2-4-c-2.png'), img4_out)  # save as ps2-4-c-2.png

def ps_5():
    # 5
    # 5-a
    # Read ps2-input1.png, compute smoothed image using a Gaussian filter
    img5 = cv2.imread(os.path.join(input_dir, 'ps2-input1.png'), 0)  # flags=0 ensures grayscale
    img5_blur = cv2.GaussianBlur(img5, (3, 3), 5)
    cv2.imwrite(os.path.join(output_dir, 'ps2-5-a-1.png'), img5_blur)  # save as ps2-5-a-1.png

    # Compute binary edge images for both original image and smoothed version
    # Lowering lower bound to get circles to show up fully
    img5_blur_edges = cv2.Canny(img5_blur, 200, 400)
    cv2.imwrite(os.path.join(output_dir, 'ps2-5-a-2.png'), img5_blur_edges)  # save as ps2-5-a-2.png

    # Apply Hough methods to smoothed image, tweak parameters to find best lines
    # Note: Write a normalized uint8 version, mapping min value to 0 and max to 255
    sobelX = cv2.Sobel(img5_blur, cv2.CV_64F, 1, 0, ksize=1)
    sobelY = cv2.Sobel(img5_blur, cv2.CV_64F, 0, 1, ksize=1)
    h_5 = hough_circles_acc(img5_blur_edges, 20, sobelX = sobelX, sobelY = sobelY)
    h_5 = cv2.normalize(h_5, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    # Find peaks (local maxima) in accumulator arrays
    peaks_5 = hough_peaks(h_5, 10, 100, 2)

    # Draw circles out around the peaks and write image to 'ps2-5-a-3.png'
    img5_a_copy = img5.copy()
    for peak in peaks_5:
        cv2.circle(img5_a_copy, (peak[0], peak[1]), 20, (0, 0, 255), 2)
    cv2.imwrite(os.path.join(output_dir, 'ps2-5-a-3.png'), img5_a_copy)

    # 5-b
    centers, radii = find_circles(img5_blur_edges, (20, 30), sobelX = sobelX, sobelY = sobelY)
    # Just had to significantly reduce the upper range

    img5_b_copy = img5.copy()
    for idx in range(len(centers)):
        for peak in centers[idx]:
            cv2.circle(img5_b_copy, (peak[0], peak[1]), radii[idx], (0, 0, 255), 2)
    cv2.imwrite(os.path.join(output_dir, 'ps2-5-b-1.png'), img5_b_copy)

def ps_6():
    # 6
    # 6-a
    img6 = cv2.imread(os.path.join(input_dir, 'ps2-input2.png'), 0)  # flags=0 ensures grayscale
    img6_blur = cv2.GaussianBlur(img6, (3, 3), 5)

    # Compute binary edge images for both original image and smoothed version
    img6_blur_edges = cv2.Canny(img6_blur, 100, 150)

    # Apply Hough methods to smoothed image, tweak parameters to find best lines
    # Store accumulator array (H) as ps2-2-a-1.png
    # Note: Write a normalized uint8 version, mapping min value to 0 and max to 255
    h_6, rho_6, theta_6 = hough_lines_acc(img6_blur_edges, smooth=False)
    h_6 = cv2.normalize(h_6, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    # Find peaks (local maxima) in accumulator arrays
    peaks_6 = hough_peaks(h_6, 25, 100, 3)

    # Draw edge lines onto image
    img6_out = cv2.cvtColor(img6, cv2.COLOR_GRAY2BGR)  # copy & convert to color image
    hough_lines_draw(img6_out, peaks_6, rho_6, theta_6)
    cv2.imwrite(os.path.join(output_dir, 'ps2-6-a-1.png'), img6_out)  # save as ps2-6-a-1.png

    # # 6-c
    img6_c_blur = cv2.GaussianBlur(img6, (3, 3), 5)
    img6_c_edges = cv2.Canny(img6_c_blur, 100, 215)

    h_c_6, rho_c_6, theta_6_c = hough_lines_acc(img6_c_edges, smooth=False)

    h_c_6 = cv2.normalize(h_c_6, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    h_c_6 = cut_short_lines(h_c_6, points, dis_threshold=4, length_threshold=40)

    peaks_6 = hough_peaks(h_c_6, 100, 50, 3)
    peaks_6_par = find_parallel_peaks(peaks_6, theta_6_c, theta_range=3, min_distance = 5, max_distance=40)

    img6_c_out = cv2.cvtColor(img6_c_blur, cv2.COLOR_GRAY2BGR)  # copy & convert to color image
    hough_lines_draw(img6_c_out, peaks_6_par, rho_c_6, theta_6_c)

    cv2.imwrite(os.path.join(output_dir, 'ps2-6-c-1.png'), img6_c_out)

def ps_7():
    # 7
    # Find circles in the same realtistic image, ps2-input2.png
    # 7-a
    # Read ps2-input1.png, compute smoothed image using a Gaussian filter
    img7 = cv2.imread(os.path.join(input_dir, 'ps2-input2.png'), 0)  # flags=0 ensures grayscale
    img7_blur = cv2.GaussianBlur(img7, (3, 3), 5)

    # Compute binary edge images for both original image and smoothed version
    # Lowering lower bound to get circles to show up fully
    img7_blur_edges = cv2.Canny(img7_blur, 100, 200)
    sobelX = cv2.Sobel(img7_blur_edges, cv2.CV_64F, 1, 0, ksize=1)
    sobelY = cv2.Sobel(img7_blur_edges, cv2.CV_64F, 0, 1, ksize=1)

    # Apply Hough methods to smoothed image, tweak parameters to find best lines
    # Note: Write a normalized uint8 version, mapping min value to 0 and max to 255
    centers, radii = find_circles(img7_blur_edges, (20, 38), dim_step = 1, peak_min = 175, peak_num = 6, sobelX = sobelX, sobelY = sobelY)
    # Find peaks (local maxima) in accumulator arrays

    img7_copy = cv2.cvtColor(img7_blur, cv2.COLOR_GRAY2BGR)  # copy & convert to color image
    for idx in range(len(centers)):
        for peak in centers[idx]:
            cv2.circle(img7_copy, (peak[0], peak[1]), radii[idx], (0, 0, 255), 2)

    cv2.imwrite(os.path.join(output_dir, 'ps2-7-a-1.png'), img7_copy)

def ps_8():
    # 8
    # Find lines and circles in distorted image, ps2-input3.png
    img8 = cv2.imread(os.path.join(input_dir, 'ps2-input3.png'), 0)  # flags=0 ensures grayscale
    img8_blur = cv2.GaussianBlur(img8, (5, 5), 10)

    img8_line_edges = cv2.Canny(img8_blur, 40, 120)
    # This lowe bound was only way to get the left side of the left pen

    img8_circle_edges = cv2.Canny(img8_blur, 40, 150)

    sobelX = cv2.Sobel(img8_circle_edges, cv2.CV_64F, 1, 0, ksize=1)
    sobelY = cv2.Sobel(img8_circle_edges, cv2.CV_64F, 0, 1, ksize=1)

    h_8, rho_8, theta_8 = hough_lines_acc(img8_line_edges, smooth=False)
    h_8 = cut_short_lines(h_8, points, dis_threshold=3, length_threshold=30)
    h_8 = cv2.normalize(h_8, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)

    centers, radii = find_circles(img8_circle_edges, (22, 35), dim_step = 1, peak_min = 50, peak_num = 3, sobelX = sobelX, sobelY = sobelY)

    # Find peaks (local maxima) in accumulator arrays
    line_peaks_8 = hough_peaks(h_8, 20, 100, 3)
    line_peaks_8 = find_parallel_peaks(line_peaks_8, theta_8, theta_range=2, min_distance=10, max_distance=40)

    img_out = cv2.cvtColor(img8_blur, cv2.COLOR_GRAY2BGR)  # copy & convert to color image
    # Draw the found lines
    hough_lines_draw(img_out, line_peaks_8, rho_8, theta_8)

    # Draw the found circles
    for idx in range(len(centers)):
        for peak in centers[idx]:
            cv2.circle(img_out, (peak[0], peak[1]), radii[idx], (0, 0, 255), 2)

    cv2.imwrite(os.path.join(output_dir, 'ps2-8-a-1.png'), img_out)


def main():
    """Run code/call functions to solve problems."""

    # print 1
    ps_1()

    # print 2
    ps_2()

    # print 3
    ps_3()

    # print 4
    ps_4()

    # print 5
    ps_5()

    # print 6
    ps_6()

    # print 7
    ps_7()

    # print 8
    ps_8()


if __name__ == "__main__":
    main()
