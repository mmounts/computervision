"""Problem Set 5: Harris, SIFT, RANSAC."""

import numpy as np
import cv2
from math import atan2, degrees, sqrt

import os

# I/O directories
input_dir = "input"
output_dir = "output"


# Assignment code
def gradientX(image):
    """Compute image gradient in X direction.

    Parameters
    ----------
        image: grayscale floating-point image, values in [0.0, 1.0]

    Returns
    -------
        Ix: image gradient in X direction, values in [-1.0, 1.0]
    """

    Ix = cv2.Sobel(image, cv2.CV_64F, 1, 0, ksize=3)
    Ix = cv2.normalize(Ix, Ix, alpha=-1.0, beta=1.0, norm_type=cv2.NORM_MINMAX)
    return Ix


def gradientY(image):
    """Compute image gradient in Y direction.

    Parameters
    ----------
        image: grayscale floating-point image, values in [0.0, 1.0]

    Returns
    -------
        Iy: image gradient in Y direction, values in [-1.0, 1.0]
    """

    Iy = cv2.Sobel(image, cv2.CV_64F, 0, 1, ksize=3)
    Iy = cv2.normalize(Iy, Iy, alpha=-1.0, beta=1.0, norm_type=cv2.NORM_MINMAX)
    return Iy


def make_image_pair(image1, image2):
    """Adjoin two images side-by-side to make a single new image.

    Parameters
    ----------
        image1: first image, could be grayscale or color (BGR)
        image2: second image, same type as first

    Returns
    -------
        image_pair: combination of both images, side-by-side, same type
    """

    # TODO: Your code here
    image_pair = np.concatenate((image1, image2), axis=1)
    return image_pair


def harris_response(Ix, Iy, kernel, alpha):
    """Compute Harris reponse map using given image gradients.

    Parameters
    ----------
        Ix: image gradient in X direction, values in [-1.0, 1.0]
        Iy: image gradient in Y direction, same size and type as Ix
        kernel: 2D windowing kernel with weights, typically square
        alpha: Harris detector parameter multiplied with square of trace

    Returns
    -------
        R: Harris response map, same size as inputs, floating-point
    """

    # Note: Define any other parameters you need locally or as keyword arguments
    R = np.zeros(Ix.shape)
    Ix2 = Ix**2;
    Iy2 = Iy**2;
    # May not need
    Ixy = Ix*Iy;
    # print Ix[0][0], Iy[0][0], Ixy[0][0], Ix2[0][0], Iy2[0][0]
    Ix2 = cv2.filter2D(Ix2, -1, kernel)
    Iy2 = cv2.filter2D(Iy2, -1, kernel)
    Ixy = cv2.filter2D(Ixy, -1, kernel) # May not need

    for x in range(Ix.shape[0]):
        for y in range(Iy.shape[1]):
            R[x][y] = (Ix2[x][y] * Iy2[x][y]) - alpha * ((Ix2[x][y] + Iy2[x][y])**2)

    # print R.max()
    return R


def find_corners(R, threshold, radius):
    """Find corners in given response map.

    Parameters
    ----------
        R: floating-point response map, e.g. output from the Harris detector
        threshold: response values less than this should not be considered plausible corners
        radius: radius of circular region for non-maximal suppression (could be half the side of square instead)

    Returns
    -------
        corners: peaks found in response map R, as a sequence (list) of (x, y) coordinates
    """
    corners = []

    max_x = R.shape[0]
    max_y = R.shape[1]

    def is_local_maxima(x, y):
        checking_val = R[x,y]
        for i in range(-radius, radius):
            for j in range(-radius, radius):
                if (i+j) > 0 and 0 < (x + i) < max_x and 0 < (y + j) < max_y:
                    if R[x+i, y+j] > checking_val:
                        return False
        return True

    for x in range(max_x):
        for y in range(max_y):
            if R[x, y] > threshold and is_local_maxima(x, y):
                corners.append([x, y])

    return corners


def draw_corners(image, corners):
    """Draw corners on (a copy of) given image.

    Parameters
    ----------
        image: grayscale floating-point image, values in [0.0, 1.0]
        corners: peaks found in response map R, as a sequence (list) of (x, y) coordinates

    Returns
    -------
        image_out: copy of image with corners drawn on it, color (BGR), uint8, values in [0, 255]
    """

    image_out = image.copy()
    for corner in corners:
        color = (np.random.randint(0,255), np.random.randint(0,255), np.random.randint(0,255))
        cv2.circle(image_out, (corner[1], corner[0]), 5, color, 2)

    return image_out


def gradient_angle(Ix, Iy):
    """Compute angle (orientation) image given X and Y gradients.

    Parameters
    ----------
        Ix: image gradient in X direction
        Iy: image gradient in Y direction, same size and type as Ix

    Returns
    -------
        angle: gradient angle image, each value in degrees [0, 359)
    """
    # Note: +ve X axis points to the right (0 degrees), +ve Y axis points down (90 degrees)
    angle = np.zeros(Ix.shape)
    for x in range(Ix.shape[0]):
        for y in range(Ix.shape[1]):
            angle[x][y] = int(degrees(atan2(Iy[x][y], Ix[x][y])))

    return angle


def get_keypoints(points, R, angle, _size, _octave=0):
    """Create OpenCV KeyPoint objects given interest points, response and angle images.

    Parameters
    ----------
        points: interest points (e.g. corners), as a sequence (list) of (x, y) coordinates
        R: floating-point response map, e.g. output from the Harris detector
        angle: gradient angle (orientation) image, each value in degrees [0, 359)
        _size: fixed _size parameter to pass to cv2.KeyPoint() for all points
        _octave: fixed _octave parameter to pass to cv2.KeyPoint() for all points

    Returns
    -------
        keypoints: a sequence (list) of cv2.KeyPoint objects
    """
    # Note: You should be able to plot the keypoints using cv2.drawKeypoints() in OpenCV 2.4.9+
    keypoints = []
    for point in points:
        keypoints.append(cv2.KeyPoint(x=point[1], y=point[0], _size=_size, _angle=angle[point[0]][point[1]], _octave=_octave))
    return keypoints


def get_descriptors(image, keypoints):
    """Extract feature descriptors from image at each keypoint.

    Parameters
    ----------
        keypoints: a sequence (list) of cv2.KeyPoint objects

    Returns
    -------
        descriptors: 2D NumPy array of shape (len(keypoints), 128)
    """

    # Note: You can use OpenCV's SIFT.compute() method to extract descriptors, or write your own!
    sift = cv2.SIFT()
    img = image.astype(np.uint8)
    keypoints, descriptors = sift.compute(img, keypoints)
    return descriptors


def match_descriptors(desc1, desc2):
    """Match feature descriptors obtained from two images.

    Parameters
    ----------
        desc1: descriptors from image 1, as returned by SIFT.compute()
        desc2: descriptors from image 2, same format as desc1

    Returns
    -------
        matches: a sequence (list) of cv2.DMatch objects containing corresponding descriptor indices
    """

    # Note: You can use OpenCV's descriptor matchers, or roll your own!
    bfm = cv2.BFMatcher()
    matches = bfm.match(desc1, desc2)
    matches = sorted(matches, key=lambda match: match.distance)
    return matches[0:40]


def draw_matches(image1, image2, kp1, kp2, matches):
    """Show matches by drawing lines connecting corresponding keypoints.

    Parameters
    ----------
        image1: first image
        image2: second image, same type as first
        kp1: list of keypoints (cv2.KeyPoint objects) found in image1
        kp2: list of keypoints (cv2.KeyPoint objects) found in image2
        matches: list of matching keypoint index pairs (as cv2.DMatch objects)

    Returns
    -------
        image_out: image1 and image2 joined side-by-side with matching lines; color image (BGR), uint8, values in [0, 255]
    """

    # Note: DO NOT use OpenCV's match drawing function(s)! Write your own :)
    img1_width = image1.shape[1]
    image_out = make_image_pair(image1, image2)
    for match in matches:
        # print match, match.queryIdx, match.trainIdx
        p1 = kp1[match.queryIdx]; p2 = kp2[match.trainIdx]
        p1 = (int(p1.pt[0]), int(p1.pt[1])); p2 = (int(p2.pt[0]) + img1_width, int(p2.pt[1]))
        # print p1, p2, match.distance
        color = (np.random.randint(0,255), np.random.randint(0,255), np.random.randint(0,255))
        cv2.line(image_out, p1, p2, color)

    return image_out


def compute_translation_RANSAC(kp1, kp2, matches):
    """Compute best translation vector using RANSAC given keypoint matches.

    Parameters
    ----------
        kp1: list of keypoints (cv2.KeyPoint objects) found in image1
        kp2: list of keypoints (cv2.KeyPoint objects) found in image2
        matches: list of matches (as cv2.DMatch objects)

    Returns
    -------
        translation: translation/offset vector <x, y>, NumPy array of shape (2, 1)
        good_matches: consensus set of matches that agree with this translation
    """
    def get_distance(p1, p2):
        p1 = (int(p1.pt[0]), int(p1.pt[1])); p2 = (int(p2.pt[0]), int(p2.pt[1]))
        xd = p2[0] - p1[0]; yd = p2[1] - p1[1]
        return sqrt(xd*xd + yd*yd), xd, yd


    good_matches = []; translation = 0
    for idx1 in range(len(matches)):
        match1 = matches[idx1]
        cur_match_set = []
        distance1, set_xd, set_yd = get_distance(kp1[match1.queryIdx], kp2[match1.trainIdx])
        cur_match_set.append(match1)
        for idx2 in range(len(matches)):
            if idx1 == idx2: continue;
            match2 = matches[idx2]
            distance2, no_xd, no_yd = get_distance(kp1[match2.queryIdx], kp2[match2.trainIdx])
            if abs(distance2 - distance1) < 80:
                cur_match_set.append(match2)
        if len(cur_match_set) > len(good_matches):
            translation = (set_xd, set_yd)
            good_matches = cur_match_set

    # print translation, good_matches
    return translation, good_matches


def compute_similarity_RANSAC(kp1, kp2, matches):
    """Compute best similarity transform using RANSAC given keypoint matches.

    Parameters
    ----------
        kp1: list of keypoints (cv2.KeyPoint objects) found in image1
        kp2: list of keypoints (cv2.KeyPoint objects) found in image2
        matches: list of matches (as cv2.DMatch objects)

    Returns
    -------
        transform: similarity transform matrix, NumPy array of shape (2, 3)
        good_matches: consensus set of matches that agree with this transform
    """

    good_matches = []; transform = None;
    for idx1 in range(len(matches)):
        match1 = matches[idx1]
        p1 = kp1[match1.queryIdx]; p1M = kp2[match1.trainIdx]
        p1 = (int(p1.pt[0]), int(p1.pt[1])); p1M = (int(p1M.pt[0]), int(p1M.pt[1]))
        for idx2 in range(len(matches)):
            match2 = matches[idx2]
            cur_matches = [match2]
            p2 = kp1[match2.queryIdx]; p2M = kp2[match2.trainIdx]
            p2 = (int(p2.pt[0]), int(p2.pt[1])); p2M = (int(p2M.pt[0]), int(p2M.pt[1]))
            M = [[p1[0], p1[1],1,0], [-p1[1], p1[0], 0, 1], [p2[0], p2[1], 1, 0], [-p2[1], p2[0], 0, 1]]
            # print M
            # M = np.linalg.inv(np.matrix(M))
            u = np.array([p1M[0], p1M[1], p2M[0], p2M[1]]); u = np.transpose(u)
            # print M, u
            v, error, rank, sin = np.linalg.lstsq(M, u)
            # print v
            T = np.array([[v[0], -v[1], v[2]], [v[1], v[0], v[3]]])
            for idx3 in range(len(matches)):
                if idx2 == idx3: continue;
                match3 = matches[idx3]
                p3 = kp1[match3.queryIdx]; p3M = kp2[match3.trainIdx]
                p3 = (int(p3.pt[0]), int(p3.pt[1])); p3M = (int(p3M.pt[0]), int(p3M.pt[1]))
                p3 = np.array([p3[0], p3[1], 1]);
                # print p3
                p3 = np.transpose(p3)
                p3_test = np.dot(T, p3)
                xd = (p3_test[0] - p3[0]); yd = (p3_test[1] - p3[1])
                dis = sqrt(xd*xd + yd*yd)
                # print dis
                if dis < 200:
                    cur_matches.append(match3)
            if len(cur_matches) > len(good_matches):
                # print good_matches
                transform = T
                good_matches = cur_matches

    return transform, good_matches


def get_image_and_gradients(filename):
    image = cv2.imread(os.path.join(input_dir, filename), cv2.IMREAD_GRAYSCALE).astype(np.float_) / 255.0
    Ix = gradientX(image)
    Iy = gradientY(image)
    image_color = cv2.imread(os.path.join(input_dir, filename))
    return image, Ix, Iy, image_color


def get_harris_response(image, Ix, Iy, kernel, alpha, outputFile):
    R = harris_response(Ix, Iy, kernel, alpha)
    R_out = None
    R_out = cv2.normalize(R, R_out, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, outputFile), R_out)
    return R


def get_harris_corners(R, threshold, radius, image, outputFile):
    corners = find_corners(R, threshold, radius)  # tweak parameters till you get good corners
    output = draw_corners(image, corners)
    # Write image to file
    cv2.imwrite(os.path.join(output_dir, outputFile), output)  # Note: you may have to scale/type-cast image before writing
    return corners


# Driver code
def main():
    # Note: Comment out parts of this code as necessary
    # Setup all images once
    transA, transA_Ix, transA_Iy, transA_color = get_image_and_gradients('transA.jpg')
    transB, transB_Ix, transB_Iy, transB_color = get_image_and_gradients('transB.jpg')
    simA, simA_Ix, simA_Iy, simA_color = get_image_and_gradients('simA.jpg')
    simB, simB_Ix, simB_Iy, simB_color = get_image_and_gradients('simB.jpg')
    # test, test_Ix, test_Iy, test_color = get_image_and_gradients('check.bmp')
    # test_rot, test_rot_Ix, test_rot_Iy, test_rot_color = get_image_and_gradients('check_rot.bmp')

    # 1a
    transA_pair = make_image_pair(transA_Ix, transA_Iy)
    transA_out = None
    transA_out = cv2.normalize(transA_pair, transA_out, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, "ps5-1-a-1.png"), transA_out)  # Note: you may have to scale/type-cast image before writing
    
    # Similarly for simA.jpg
    simA_pair = make_image_pair(simA_Ix, simA_Iy)
    simA_out = None
    simA_out = cv2.normalize(simA_pair, simA_out, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, "ps5-1-a-2.png"), simA_out)  # Note: you may have to scale/type-cast image before writing

    # 1b
    kernel = np.ones((3, 3), dtype=np.float_) / 9.0
    # kernel = np.array([[1,1,1],[1,3,1],[1,1,1]])
    # kernel = np.array([[0.3,0.5,0.3],[0.5,1,0.5],[0.3,0.5,0.3]])
    # kernel = np.array([[0.3,0.5,0.3],[0.5,1,0.5],[0.3,0.5,0.3]])
    alpha = 0.002
    transA_R = get_harris_response(transA, transA_Ix, transA_Iy, kernel, alpha, 'ps5-1-b-1.png')  # TODO: implement this, tweak parameters for best response
    # Scale/type-cast response map and write to file

    # Similarly for transB, simA and simB (you can write a utility function for grouping operations on each image)
    transB_R = get_harris_response(transB, transB_Ix, transB_Iy, kernel, alpha, 'ps5-1-b-2.png')

    simA_R = get_harris_response(simA, simA_Ix, simA_Iy, kernel, alpha, 'ps5-1-b-3.png')

    simB_R = get_harris_response(simB, simB_Ix, simB_Iy, kernel, alpha, 'ps5-1-b-4.png')

    # test_R = get_harris_response(test, test_Ix, test_Iy, kernel, alpha, 'ps5-1-b_test.png')
    # test_rot_R = get_harris_response(test_rot, test_rot_Ix, test_rot_Iy, kernel, alpha, 'ps5-1-b_test_rot.png')

    # 1c
    # print transA_R.max()
    threshold = .03; radius = 2
    transA_corners = get_harris_corners(transA_R, threshold, radius, transA_color, 'ps5-1-c-1.png')
    transB_corners = get_harris_corners(transB_R, threshold, radius, transB_color, 'ps5-1-c-2.png')
    simA_corners = get_harris_corners(simA_R, threshold, radius, simA_color, 'ps5-1-c-3.png')
    simB_corners = get_harris_corners(simB_R, threshold, radius, simB_color, 'ps5-1-c-4.png')
    # test_corners = get_harris_corners(test_R, .5, radius, test_color, 'ps5-1-c_test.png')
    # test_rot_corners = get_harris_corners(test_rot_R, 1.5, radius, test_rot_color, 'ps5-1-c_test_rot.png')


    # TODO: Similarly for transB, simA and simB (write a utility function if you want)
    # Still todo this, moving on for now though

    # 2a
    size = 10.0; octave = 0;
    transA_angle = gradient_angle(transA_Ix, transA_Iy)
    transA_kp = get_keypoints(transA_corners, transA_R, transA_angle, _size=size, _octave=octave)
    transA_kp_out = None
    transA_kp_out = cv2.drawKeypoints(transA_color, transA_kp, transA_kp_out, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # Draw keypoints on transA
    # Similarly, find keypoints for transB and draw them
    transB_angle = gradient_angle(transB_Ix, transB_Iy)
    transB_kp = get_keypoints(transB_corners, transB_R, transB_angle, _size=size, _octave=octave)
    transB_kp_out = None
    transB_kp_out = cv2.drawKeypoints(transB_color, transB_kp, transB_kp_out, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # Combine transA and transB images (with keypoints drawn) using make_image_pair() and write to file
    trans_kp_pair = make_image_pair(transA_kp_out, transB_kp_out)
    cv2.imwrite(os.path.join(output_dir, 'ps5-2-a-1.png'), trans_kp_pair)

    # Ditto for (simA, simB) pair
    simA_angle = gradient_angle(simA_Ix, simA_Iy)
    simA_kp = get_keypoints(simA_corners, simA_R, simA_angle, _size=size, _octave=octave)
    simA_kp_out = None
    simA_kp_out = cv2.drawKeypoints(simA_color, simA_kp, simA_kp_out, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # TODO: Draw keypoints on simA
    # TODO: Similarly, find keypoints for simB and draw them
    simB_angle = gradient_angle(simB_Ix, simB_Iy)
    simB_kp = get_keypoints(simB_corners, simB_R, simB_angle, _size=size, _octave=octave)
    simB_kp_out = None
    simB_kp_out = cv2.drawKeypoints(simB_color, simB_kp, simB_kp_out, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # TODO: Combine simA and simB images (with keypoints drawn) using make_image_pair() and write to file
    sim_kp_pair = make_image_pair(simA_kp_out, simB_kp_out)
    cv2.imwrite(os.path.join(output_dir, 'ps5-2-a-2.png'), sim_kp_pair)
    # print len(transA_kp), len(transB_kp), len(simA_kp), len(simB_kp)

    # test_rot_angle = gradient_angle(test_rot_Ix, test_rot_Iy)
    # test_rot_kp = get_keypoints(test_rot_corners, test_rot_R, test_rot_angle, _size=size, _octave=octave)
    # test_rot_kp_out = None
    # test_rot_kp_out = cv2.drawKeypoints(test_rot_color, test_rot_kp, test_rot_kp_out, flags=cv2.DRAW_MATCHES_FLAGS_DRAW_RICH_KEYPOINTS)
    # cv2.imwrite(os.path.join(output_dir, 'ps5-2-a_test_rot.png'), test_rot_kp_out)


    # 2b
    transA_desc = get_descriptors(transA, transA_kp)
    # Similarly get transB_desc
    transB_desc = get_descriptors(transB, transB_kp)
    # print len(transA_desc), len(transB_desc)
    # Find matches
    trans_matches = match_descriptors(transA_desc, transB_desc)
    # Draw matches and write to file
    trans_match_out = draw_matches(transA_color, transB_color, transA_kp, transB_kp, trans_matches)
    cv2.imwrite(os.path.join(output_dir, 'ps5-2-b-1.png'), trans_match_out)

    # Ditto for (simA, simB) pair (may have to vary some parameters along the way?)
    simA_desc = get_descriptors(simA, simA_kp)
    simB_desc = get_descriptors(simB, simB_kp)
    # print len(transA_desc), len(transB_desc)
    sim_matches = match_descriptors(simA_desc, simB_desc)
    sim_matches = sim_matches[0:25]
    sim_match_out = draw_matches(simA_color, simB_color, simA_kp, simB_kp, sim_matches)
    cv2.imwrite(os.path.join(output_dir, 'ps5-2-b-2.png'), sim_match_out)

    # 3a
    # Compute translation vector using RANSAC for (transA, transB) pair, draw biggest consensus set
    translation, trans_good_matches = compute_translation_RANSAC(transA_kp, transB_kp, trans_matches)
    trans_good_match_out = draw_matches(transA_color, transB_color, transA_kp, transB_kp, trans_good_matches)
    # cv2.imwrite(os.path.join(output_dir, 'ps5-3-a-1.png'), trans_good_match_out)
    print 'Translation  ', translation
    print len(trans_matches), len(trans_good_matches)

    # 3b
    # Compute similarity transform for (simA, simB) pair, draw biggest consensus set
    # print len(sim_matches)
    transform, sim_good_matches = compute_similarity_RANSAC(simA_kp, simB_kp, sim_matches)
    sim_good_match_out = draw_matches(transA_color, transB_color, transA_kp, transB_kp, trans_good_matches)
    print 'Transform  ', transform
    print len(sim_matches), len(sim_good_matches)
    # print len(sim_good_matches)
    # cv2.imwrite(os.path.join(output_dir, 'ps5-3-b-1.png'), sim_good_match_out)


    # Extra credit: 3c, 3d, 3e


if __name__ == "__main__":
    main()
