"""Problem Set 3: Window-based Stereo Matching."""

import numpy as np
import cv2
import math
import sys

import os

input_dir = "input"  # read images from os.path.join(input_dir, <filename>)
output_dir = "output"  # write images to os.path.join(output_dir, <filename>)

def disparity_ssd(L, R, template_size=3):
    """Compute disparity map D(y, x) such that: L(y, x) = R(y, x + D(y, x))
    
    Params:
    L: Grayscale left image, in range [0.0, 1.0]
    R: Grayscale right image, same size as L

    Returns: Disparity map, same size as L, R
    """
    center_offset = int(math.floor(template_size/2))

    # Set up strides for faster processing
    # Set up R strides
    r_copy = cv2.copyMakeBorder(R, center_offset, center_offset, center_offset, center_offset, cv2.BORDER_CONSTANT, value=0)
    l_copy = cv2.copyMakeBorder(L, center_offset, center_offset, center_offset, center_offset, cv2.BORDER_CONSTANT, value=0)

    itemsize = r_copy.itemsize
    r_x, r_y = r_copy.shape
    r_stride_shape = (R.shape[0], R.shape[1], template_size, template_size)
    r_strides = (r_y * itemsize, itemsize, r_y * itemsize, itemsize)
    r_blocks = np.lib.stride_tricks.as_strided(r_copy, shape=r_stride_shape, strides=r_strides)

    results = np.zeros(L.shape)
    max_travel = math.ceil(l_copy.shape[1] / 4)

    for j in range(center_offset, l_copy.shape[0] - center_offset):
        r_row = r_blocks[j-center_offset:j-center_offset+1, :]
        for i in range(center_offset, l_copy.shape[1] - center_offset):
            l_block = l_copy[j-center_offset:j+center_offset+1, i-center_offset:i+center_offset+1]

            # Calculate sum squares for current block against all blocks in the moved image row
            squares = ((l_block - r_row)**2)[0]
            sum_squares = np.sum(squares, axis=1)
            sum_squares = np.sum(sum_squares, axis=1)
            # Set center
            px_result = (np.argmin(sum_squares) - i + center_offset)
            px_result = 0 if abs(px_result) > max_travel else px_result
            results[j-center_offset, i-center_offset] = px_result

    return results


def disparity_ncorr(L, R, template_size=3):
    """Compute disparity map D(y, x) such that: L(y, x) = R(y, x + D(y, x))
    
    Params:
    L: Grayscale left image, in range [0.0, 1.0]
    R: Grayscale right image, same size as L

    Returns: Disparity map, same size as L, R
    """
    center_offset = int(math.floor(template_size/2))

    # Set up strides for faster processing
    # Set up R strides
    r_copy = cv2.copyMakeBorder(R, center_offset, center_offset, center_offset, center_offset, cv2.BORDER_CONSTANT, value=0)
    l_copy = cv2.copyMakeBorder(L, center_offset, center_offset, center_offset, center_offset, cv2.BORDER_CONSTANT, value=0)

    itemsize = r_copy.itemsize
    r_x, r_y = r_copy.shape
    r_stride_shape = (R.shape[0], R.shape[1], template_size, template_size)
    r_strides = (r_y * itemsize, itemsize, r_y * itemsize, itemsize)
    r_blocks = np.lib.stride_tricks.as_strided(r_copy, shape=r_stride_shape, strides=r_strides)

    results = np.zeros(L.shape)
    max_travel = math.ceil(l_copy.shape[1] / 4)

    for j in range(center_offset, l_copy.shape[0] - center_offset):
        r_strip = r_copy[j-center_offset:j+center_offset+1, :]
        for i in range(center_offset, l_copy.shape[1] - center_offset):
            l_block = l_copy[j-center_offset:j+center_offset+1, i-center_offset:i+center_offset+1]
            # Calculate normalized correlation for current block against all blocks in the moved image row
            n_corrs = cv2.matchTemplate(r_strip.astype(np.float32), l_block.astype(np.float32), method=cv2.TM_CCORR_NORMED)
            # Set center
            px_result = (np.argmax(n_corrs) - i - center_offset)
            px_result = 0 if abs(px_result) > max_travel else px_result
            results[j-center_offset, i-center_offset] = px_result

    return results


def p1():
    # 1-a
    # Read images
    L = cv2.imread(os.path.join('input', 'pair0-L.png'), 0) * (1 / 255.0)  # grayscale, scale to [0.0, 1.0]
    R = cv2.imread(os.path.join('input', 'pair0-R.png'), 0) * (1 / 255.0)

    np.set_printoptions(threshold=np.nan)
    # Compute disparity (using method disparity_ssd defined in disparity_ssd.py)
    D_L = disparity_ssd(L, R, 13)  # implemenet disparity_ssd()
    D_R = disparity_ssd(R, L, 13)

    # Save output images (D_L as output/ps3-1-a-1.png and D_R as output/ps3-1-a-2.png)
    cv2.normalize(D_L, D_L, 0, 255, cv2.NORM_MINMAX)
    cv2.normalize(D_R, D_R, 0, 255, cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, 'ps3-1-a-1.png'), D_L)
    cv2.imwrite(os.path.join(output_dir, 'ps3-1-a-2.png'), D_R)

    # Note: They may need to be scaled/shifted before saving to show results properly


def p2():
    # 2
    # TODO: Apply disparity_ssd() to pair1-L.png and pair1-R.png (in both directions)
    L = cv2.imread(os.path.join('input', 'pair1-L.png'), 0) * (1 / 255.0)  # grayscale, scale to [0.0, 1.0]
    R = cv2.imread(os.path.join('input', 'pair1-R.png'), 0) * (1 / 255.0)  # grayscale, scale to [0.0, 1.0]

    D_L = disparity_ssd(L, R, 13)
    D_R = disparity_ssd(R, L, 13)

    # Save output images (D_L as output/ps3-2-a-1.png and D_R as output/ps3-2-a-2.png)
    cv2.normalize(D_L, D_L, 0, 255, cv2.NORM_MINMAX)
    cv2.normalize(D_R, D_R, 0, 255, cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, 'ps3-2-a-1.png'), D_L)
    cv2.imwrite(os.path.join(output_dir, 'ps3-2-a-2.png'), D_R)


def p3():
    # 3
    # Apply disparity_ssd() to noisy versions of pair1 images
    L = cv2.imread(os.path.join('input', 'pair1-L.png'), 0) * (1 / 255.0)  # grayscale, scale to [0.0, 1.0]
    R = cv2.imread(os.path.join('input', 'pair1-R.png'), 0) * (1 / 255.0)  # grayscale, scale to [0.0, 1.0]

    # Add noise to L
    noise = np.random.normal(0, .05, L.shape)
    l_noise = np.add(L, noise)

    # cv2.normalize(l_noise, l_noise, 0, 255, cv2.NORM_MINMAX)
    # cv2.imwrite(os.path.join(output_dir, 'ps3-3-a-1-noisy.png'), l_noise)
    D_L = disparity_ssd(l_noise, R, 15)
    D_R = disparity_ssd(R, l_noise, 15)

    # Save output images (D_L as output/ps3-2-a-1.png and D_R as output/ps3-2-a-2.png)
    cv2.normalize(D_L, D_L, 0, 255, cv2.NORM_MINMAX)
    cv2.normalize(D_R, D_R, 0, 255, cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, 'ps3-3-a-1.png'), D_L)
    cv2.imwrite(os.path.join(output_dir, 'ps3-3-a-2.png'), D_R)

    # Boost contrast in one image and apply again
    # Check that the 'boost' is actually changing something
    l_boost = L.copy() * 1.1
    l_boost = cv2.normalize(l_boost, l_boost, 0.0, 1.0, cv2.NORM_MINMAX)

    D_L = disparity_ssd(l_boost, R, 13)
    D_R = disparity_ssd(R, l_boost, 13)

    cv2.normalize(D_L, D_L, 0, 255, cv2.NORM_MINMAX)
    cv2.normalize(D_R, D_R, 0, 255, cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, 'ps3-3-b-1.png'), D_L)
    cv2.imwrite(os.path.join(output_dir, 'ps3-3-b-2.png'), D_R)


def p4():
    # 4
    # Implement disparity_ncorr() and apply to pair1 images (original, noisy and contrast-boosted)

    # 4.a
    L = cv2.imread(os.path.join('input', 'pair1-L.png'), 0) * (1 / 255.0)  # grayscale, scale to [0.0, 1.0]
    R = cv2.imread(os.path.join('input', 'pair1-R.png'), 0) * (1 / 255.0)  # grayscale, scale to [0.0, 1.0]

    D_L = disparity_ncorr(L, R, 13)
    D_R = disparity_ncorr(R, L, 13)

    cv2.normalize(D_L, D_L, 0, 255, cv2.NORM_MINMAX)
    cv2.normalize(D_R, D_R, 0, 255, cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, 'ps3-4-a-1.png'), D_L)
    cv2.imwrite(os.path.join(output_dir, 'ps3-4-a-2.png'), D_R)

    # 4.b
    # Noise
    l_noisy = np.add(L.copy(), np.random.normal(0, .05, L.shape))

    D_L_n = disparity_ncorr(l_noisy, R, 13)
    D_R_n = disparity_ncorr(R, l_noisy, 13)

    cv2.normalize(D_L_n, D_L_n, 0, 255, cv2.NORM_MINMAX)
    cv2.normalize(D_R_n, D_R_n, 0, 255, cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, 'ps3-4-b-1.png'), D_L_n)
    cv2.imwrite(os.path.join(output_dir, 'ps3-4-b-2.png'), D_R_n)


    # Boost
    l_boost = L.copy() * 1.1
    l_boost = cv2.normalize(l_boost, l_boost, 0.0, 1.0, cv2.NORM_MINMAX)

    D_L_b = disparity_ncorr(l_boost, R, 13)
    D_R_b = disparity_ncorr(R, l_boost, 13)

    cv2.normalize(D_L_b, D_L_b, 0, 255, cv2.NORM_MINMAX)
    cv2.normalize(D_R_b, D_R_b, 0, 255, cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, 'ps3-4-b-3.png'), D_L_b)
    cv2.imwrite(os.path.join(output_dir, 'ps3-4-b-4.png'), D_R_b)



def p5():
    # 5
    # TODO: Apply stereo matching to pair2 images, try pre-processing the images for best results

    # 5.a
    L = cv2.imread(os.path.join('input', 'pair2-L.png'), 0) * (1 / 255.0)  # grayscale, scale to [0.0, 1.0]
    R = cv2.imread(os.path.join('input', 'pair2-R.png'), 0) * (1 / 255.0)  # grayscale, scale to [0.0, 1.0]

    l_boost = L.copy() * 1.1
    l_boost = cv2.normalize(l_boost, l_boost, 0.0, 1.0, cv2.NORM_MINMAX)

    D_L2 = disparity_ncorr(l_boost, R, 13)
    D_R2 = disparity_ncorr(R, l_boost, 13)

    cv2.normalize(D_L2, D_L2, 0, 255, cv2.NORM_MINMAX)
    cv2.normalize(D_R2, D_R2, 0, 255, cv2.NORM_MINMAX)
    cv2.imwrite(os.path.join(output_dir, 'ps3-5-a-1.png'), D_L2)
    cv2.imwrite(os.path.join(output_dir, 'ps3-5-a-2.png'), D_R2)


def main():
    """Run code/call functions to solve problems."""

    # p1()

    # p2()

    # p3()

    # p4()

    p5()


def test():
    # L = np.arange(36).reshape(6, 6)
    # R = np.arange(36).reshape(6, 6)
    L = np.arange(48).reshape(6, 8)
    R = np.arange(48).reshape(6, 8)

    R[2,1] = 14
    R[2,2] = 15
    R[2,3] = 16
    R[2,4] = 16
    R[3,1] = 20
    R[3,2] = 21
    R[3,3] = 22
    R[3,4] = 22
    R[4,1] = 26
    R[4,2] = 27
    R[4,3] = 28
    R[4,4] = 28

    # print L
    # print R

    D_L = disparity_ssd(L, R, 3)
    # D_L = disparity_ncorr(L.astype(np.float32), R.astype(np.float32), 3)

    # print D_L


if __name__ == "__main__":
    main()
